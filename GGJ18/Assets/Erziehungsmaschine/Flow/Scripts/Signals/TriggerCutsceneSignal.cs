﻿using System;
using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class TriggerCutsceneSignal : Signal<TriggerCutsceneSignal, Cutscene, Action>
    {
    }
}