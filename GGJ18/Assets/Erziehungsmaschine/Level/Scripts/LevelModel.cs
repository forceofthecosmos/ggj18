using System;
using System.Collections.Generic;
using Erziehungsmaschine.Humans;

namespace Erziehungsmaschine.Level
{
    public class LevelGoal
    {
        public HumanAttributeCollection NeededAttributes;

        public int NeededCount;

        public int CurrentCount;

        public bool IsFulfilled
        {
            get { return CurrentCount >= NeededCount; }
        }
    }

    public class LevelModel
    {
        public event Action ScoreChanged;

        public event Action ConfigChanged;

        public List<LevelGoal> Goals;

        public LevelConfigScriptableObject Config
        {
            get { return config; }
            set
            {
                config = value;
                FireConfigChanged();
            }
        }
        
        public int WrongHumanCount
        {
            get { return wrongHumanCount; }
            set
            {
                wrongHumanCount = value; 
                FireScoreChanged();
            }
        }

        private int wrongHumanCount;

        /// <summary>
        ///   Indicates if current level is finished.
        /// </summary>
        public bool IsLevelFinished;

        public event Action<bool> GameOver;

        private LevelConfigScriptableObject config;

        public bool HasWon()
        {
            foreach (var levelGoal in Goals)
            {
                if (!levelGoal.IsFulfilled)
                {
                    return false;
                }
            }

            return true;
        }

        public bool HasLost()
        {
            return WrongHumanCount >= Config.WrongHumanFailCount;
        }

        public virtual void FireScoreChanged()
        {
            Action handler = ScoreChanged;
            if (handler != null) handler();
        }

        protected virtual void FireConfigChanged()
        {
            Action handler = ConfigChanged;
            if (handler != null) handler();
        }

        public virtual void OnGameOver(bool wonGame)
        {
            var handler = GameOver;
            if (handler != null) handler(wonGame);
        }
    }
}