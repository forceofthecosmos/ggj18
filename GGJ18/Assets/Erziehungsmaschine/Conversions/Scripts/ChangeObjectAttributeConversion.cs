﻿using Erziehungsmaschine.Humans;

namespace Erziehungsmaschine.Conversions
{
    public class ChangeObjectAttributeConversion : ChangeAttributeConversion<HumanObjectAttribute>
    {
        public ChangeObjectAttributeConversion() : base((attributes, value) => attributes.ObjectAttribute = value,
            attributes => attributes.ObjectAttribute)
        {
        }

        protected override HumanObjectAttribute Wildcard
        {
            get { return HumanObjectAttribute.Any; }
        }
    }
}