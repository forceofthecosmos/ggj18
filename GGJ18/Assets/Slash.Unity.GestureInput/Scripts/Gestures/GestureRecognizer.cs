namespace Slash.Unity.GestureInput.Gestures
{
    using System;
    using System.Collections.Generic;
    using Slash.Unity.GestureInput.Modules;
    using UnityEngine;

    public abstract class GestureRecognizer<TEventData> : GestureRecognizer
        where TEventData : IGestureData
    {
        protected void OnGestureDetected(TEventData eventData)
        {
            base.OnGestureDetected(eventData);
        }
    }

    public abstract class GestureRecognizer
    {
        public event Action<IGestureData> GestureDetected;

        public event Action<IGestureData, Vector2> GestureDetectedAtPosition;

        public event Action<IGestureData, IEnumerable<GameObject>> GestureDetectedForReceivers;

        public abstract IEnumerable<RegisteredInputSource> GetInputSources();

        /// <summary>
        ///   Per frame processing of input.
        /// </summary>
        public abstract void Process();

        protected void OnGestureDetected(IGestureData eventData)
        {
            var handler = this.GestureDetected;
            if (handler != null)
            {
                handler(eventData);
            }
        }

        protected void OnGestureDetected(IGestureData eventData, IEnumerable<GameObject> receivers)
        {
            var handler = this.GestureDetectedForReceivers;
            if (handler != null)
            {
                handler(eventData, receivers);
            }
        }

        protected void OnGestureDetected(IGestureData eventData, Vector2 position)
        {
            var handler = this.GestureDetectedAtPosition;
            if (handler != null)
            {
                handler(eventData, position);
            }
        }
    }
}