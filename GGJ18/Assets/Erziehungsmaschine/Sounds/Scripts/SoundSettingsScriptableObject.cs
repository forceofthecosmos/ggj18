﻿using System;
using System.IO;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Sounds.Scripts
{
    public class SoundSettingsScriptableObject : ScriptableObjectInstaller<SoundSettingsScriptableObject>
    {
        public AudioSettings Audio;
        
        [Serializable]
        public class AudioSettings
        {
            public AudioClip MainTheme;
    
            public AudioClip MainThemeLoop;

            public AudioClip CrowdBig;

            public AudioClip CrowdSmall;

            public AudioClip DropBackInGame;

            public AudioClip DropOnConstruct;

            public AudioClip GetReady;

            public AudioClip KaChing;

            public AudioClip Yay;
        }

        public override void InstallBindings()
        {
            Container.BindInstance(Audio);
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Tools/Create Sound Config")]
        public static void Create()
        {
            var obj = CreateInstance<SoundSettingsScriptableObject>();
            var selectedPath = UnityEditor.AssetDatabase.GUIDToAssetPath(UnityEditor.Selection.assetGUIDs[0]);
            if (string.IsNullOrEmpty(selectedPath))
            {
                Debug.LogError("Select an directory");
                return;
            }

            var assetPath = Path.Combine(selectedPath, "SoundConfig.asset");
            UnityEditor.AssetDatabase.CreateAsset(obj, assetPath);
        }
#endif
    }
}