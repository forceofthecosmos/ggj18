﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Erziehungsmaschine.Flow
{
    [Serializable]
    public class CutsceneStep
    {
        [TextArea(3, 10)]
        public string Text;

        /// <summary>
        ///   Id of construction to unlock.
        /// </summary>
        public string UnlockConstructionId;
    }
    
    [Serializable]
    public class Cutscene
    {
        public List<CutsceneStep> Steps;
    }
}