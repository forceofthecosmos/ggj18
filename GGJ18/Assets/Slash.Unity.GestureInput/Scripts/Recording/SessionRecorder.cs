﻿namespace Slash.Unity.GestureInput.Recording
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Slash.Unity.GestureInput.Gestures;
    using Slash.Unity.GestureInput.Modules;
    using Slash.Unity.GestureInput.Utils;

    using UnityEngine;

    public class SessionRecorder : MonoBehaviour
    {
        public string FileName = "Session";

        public GestureInputModule InputModule;

        private SessionKey lastKey;

        public Transform PlayerTransform;

        private Session recordingSession;

        [ContextMenu("Start Recording")]
        public void StartRecording()
        {
            if (this.InputModule == null)
            {
                Debug.LogWarning("No input module set", this);
                return;
            }

            this.lastKey = new SessionKey() { Name = "Start", Time = Time.time };
            this.recordingSession = new Session
            {
                Gestures = new List<SessionGesture>(),
                PlayerTransform =
                    this.PlayerTransform != null ? this.PlayerTransform.gameObject.GetPath() : string.Empty,
                Keys = new List<SessionKey>() { this.lastKey }
            };

            this.InputModule.GestureDetected += this.OnGestureDetected;
        }

        public void AddKey(string keyName)
        {
            if (this.recordingSession == null)
            {
                return;
            }

            this.lastKey = new SessionKey() { Name = keyName, Time = Time.time };
            this.recordingSession.Keys.Add(this.lastKey);
        }

        [ContextMenu("Stop Recording")]
        public void StopRecording()
        {
            if (this.recordingSession == null)
            {
                Debug.LogWarning("No recording running", this);
                return;
            }

            // Write to file.
            var sessionContents = JsonUtility.ToJson(this.recordingSession);
            File.WriteAllText(this.FileName, sessionContents);

            this.recordingSession = null;
        }

        private void OnGestureDetected(IGestureData gestureData, List<GameObject> pointedGameObjects)
        {
            var gestureType = gestureData.GetType();
            var sessionGesture = new SessionGesture
            {
                Type = gestureType.FullName,
                Key = this.lastKey.Name,
                RelativeTime = Time.time - this.lastKey.Time,
                Data = JsonUtility.ToJson(gestureData),
                Receivers = pointedGameObjects.Select(pointedGameObject => pointedGameObject.GetPath()).ToList()
            };

            if (this.PlayerTransform != null)
            {
                sessionGesture.PlayerOrientation = this.PlayerTransform.rotation;
            }

            this.recordingSession.Gestures.Add(sessionGesture);
        }
    }
}