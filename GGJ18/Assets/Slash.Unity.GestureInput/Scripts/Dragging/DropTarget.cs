﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Slash.Unity.GestureInput.Dragging
{
    public class DropTarget : MonoBehaviour, IEventSystemHandler
    {
        public DropEvent Drop;

        public bool OnDrop(DropEventData data)
        {
            Drop.Invoke(data);
            return true;
        }

        [Serializable]
        public class DropEvent : UnityEvent<DropEventData>
        {
        }
    }
}