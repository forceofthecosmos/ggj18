﻿using Erziehungsmaschine.Humans;
using Spine.Unity;
using TMPro;
using UnityEngine;

namespace Erziehungsmaschine.Level
{
    public class LevelGoalView : MonoBehaviour
    {
        public SkeletonAnimation SkeletonAnimation;

        public TextMeshProUGUI TextCounter;

        public void Setup(LevelGoalConfig goal)
        {
            this.gameObject.SetActive(goal != null);

            if (goal != null && SkeletonAnimation != null)
            {
                HumanView.UpdateAttributeSprites(SkeletonAnimation.Skeleton, goal.NeededAttributes);
                SkeletonAnimation.AnimationState.SetAnimation(1,
                    goal.NeededAttributes.ObjectAttribute != HumanObjectAttribute.Default
                        ? "add-object"
                        : "remove-object",
                    true);
            }
        }

        public void UpdateScore(LevelGoal levelGoal)
        {
            if (TextCounter != null)
            {
                TextCounter.text = levelGoal != null
                    ? string.Format("{0}/{1}", levelGoal.CurrentCount, levelGoal.NeededCount)
                    : string.Empty;
            }
        }
    }
}