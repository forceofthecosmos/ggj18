﻿using Slash.Unity.GestureInput.Gestures;
using UnityEditor;
using UnityEngine;

namespace Slash.Unity.GestureInput.Editor.Gestures
{
    using Slash.Unity.GestureInput.Gestures.Implementations;

    [CustomPropertyDrawer(typeof (TriggerGestureRecognizer.Trigger))]
    public class TriggerGestureRecognizerTriggerPropertyDrawer : PropertyDrawer
    {
        private SerializedProperty modeProperty;

        private SerializedProperty moveThresholdProperty;

        private SerializedProperty pressDurationMaxProperty;

        private SerializedProperty pressDurationMinProperty;

        private TriggerGestureRecognizer.Trigger.TriggerMode triggerMode;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            this.modeProperty = property.FindPropertyRelative("Mode");
            this.moveThresholdProperty = property.FindPropertyRelative("MoveThreshold");
            this.pressDurationMinProperty = property.FindPropertyRelative("PressDurationMin");
            this.pressDurationMaxProperty = property.FindPropertyRelative("PressDurationMax");
            this.triggerMode = (TriggerGestureRecognizer.Trigger.TriggerMode) this.modeProperty.enumValueIndex;
            var additionalHeight = 0.0f;
            if (property.isExpanded)
            {
                additionalHeight += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("Name"));
                additionalHeight += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("TriggerButton"));
                additionalHeight += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("Mode"));
                if (this.triggerMode == TriggerGestureRecognizer.Trigger.TriggerMode.Release)
                {
                    additionalHeight += EditorGUI.GetPropertyHeight(this.moveThresholdProperty);
                    additionalHeight += EditorGUI.GetPropertyHeight(this.pressDurationMinProperty);
                    additionalHeight += EditorGUI.GetPropertyHeight(this.pressDurationMaxProperty);
                }
            }
            return base.GetPropertyHeight(property, label) + additionalHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);

            var basePosition = position;
            basePosition.height = base.GetPropertyHeight(property, label);
            EditorGUI.PropertyField(basePosition, property);
            position.y += basePosition.height;

            if (property.isExpanded)
            {
                position = DrawDefaultChildProperty(position, property, "Name");

                position = DrawDefaultChildProperty(position, property, "TriggerButton");

                position = DrawDefaultChildProperty(position, property, "Mode");

                if (this.triggerMode == TriggerGestureRecognizer.Trigger.TriggerMode.Release)
                {
                    var moveThresholdPosition = position;
                    moveThresholdPosition.height = EditorGUI.GetPropertyHeight(this.moveThresholdProperty);
                    EditorGUI.PropertyField(moveThresholdPosition, this.moveThresholdProperty);
                    position.y += moveThresholdPosition.height;

                    var pressDurationMinPosition = position;
                    pressDurationMinPosition.height = EditorGUI.GetPropertyHeight(this.pressDurationMinProperty);
                    EditorGUI.PropertyField(pressDurationMinPosition, this.pressDurationMinProperty);
                    position.y += pressDurationMinPosition.height;

                    var pressDurationMaxPosition = position;
                    pressDurationMaxPosition.height = EditorGUI.GetPropertyHeight(this.pressDurationMaxProperty);
                    EditorGUI.PropertyField(pressDurationMaxPosition, this.pressDurationMaxProperty);
                    position.y += pressDurationMaxPosition.height;
                }
            }

            EditorGUI.EndProperty();
        }

        private static Rect DrawDefaultChildProperty(Rect position, SerializedProperty property, string name)
        {
            var childProperty = property.FindPropertyRelative(name);
            var childPosition = position;
            childPosition.height = EditorGUI.GetPropertyHeight(childProperty);
            EditorGUI.PropertyField(childPosition, childProperty);
            position.y += childPosition.height;
            return position;
        }
    }
}