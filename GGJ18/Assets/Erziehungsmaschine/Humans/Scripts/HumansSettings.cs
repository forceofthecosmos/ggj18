﻿using UnityEngine;

namespace Erziehungsmaschine.Humans
{
    public class HumansSettings : MonoBehaviour
    {
        public GameObject SpawnPosition;
        
        public GameObject HumanEndPosition;
        
        public float HumanSpawnTime;

        public GameObject HumanPrefab;

        public GameObject HumansContainer;

        public GameObject[] HumansWalkPoints;
    }
}