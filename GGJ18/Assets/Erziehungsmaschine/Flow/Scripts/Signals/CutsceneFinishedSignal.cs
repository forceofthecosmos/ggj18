﻿using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class CutsceneFinishedSignal : Signal<CutsceneFinishedSignal>
    {
    }
}