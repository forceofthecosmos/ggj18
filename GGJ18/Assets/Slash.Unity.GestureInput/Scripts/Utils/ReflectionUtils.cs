﻿namespace Slash.Unity.GestureInput.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public static class ReflectionUtils
    {
        /// <summary>
        ///   Gets all assemblies that are loaded in the current application domain.
        /// </summary>
        /// <returns>All loaded assemblies.</returns>
        public static IEnumerable<Assembly> GetLoadedAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies();
        }

        /// <summary>
        ///   Removes all assembly info from the specified type name.
        /// </summary>
        /// <param name="typeName">Type name to remove assembly info from.</param>
        /// <returns>Type name without assembly info.</returns>
        public static string RemoveAssemblyInfo(string typeName)
        {
            // Get start of "Version=..., Culture=..., PublicKeyToken=..." string.
            int versionIndex = typeName.IndexOf("Version=", StringComparison.Ordinal);

            if (versionIndex >= 0)
            {
                // Get end of "Version=..., Culture=..., PublicKeyToken=..." string for generics.
                int endIndex = typeName.IndexOf(']', versionIndex);

                // Get end of "Version=..., Culture=..., PublicKeyToken=..." string for non-generics.
                endIndex = endIndex >= 0 ? endIndex : typeName.Length;

                // Remove version info.
                typeName = typeName.Remove(versionIndex - 2, endIndex - versionIndex + 2);
            }

            return typeName;
        }

        /// <summary>
        ///   <para>
        ///     Looks up the specified full type name in all loaded assemblies,
        ///     ignoring assembly version.
        ///   </para>
        ///   <para>
        ///     In order to understand how to access generic types,
        ///     see http://msdn.microsoft.com/en-us/library/w3f99sx1.aspx.
        ///   </para>
        /// </summary>
        /// <param name="fullName">Full name of the type to find.</param>
        /// <returns>Type with the specified name.</returns>
        /// <exception cref="TypeLoadException">If the type couldn't be found.</exception>
        public static Type FindType(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                return null;
            }

            // Split type name from .dll version.
            fullName = RemoveAssemblyInfo(fullName);

            Type t = Type.GetType(fullName);

            if (t != null)
            {
                return t;
            }

            foreach (Assembly asm in GetLoadedAssemblies())
            {
                t = asm.GetType(fullName);
                if (t != null)
                {
                    return t;
                }
            }

            throw new TypeLoadException(string.Format("Unable to find type {0}.", fullName));
        }
    }
}