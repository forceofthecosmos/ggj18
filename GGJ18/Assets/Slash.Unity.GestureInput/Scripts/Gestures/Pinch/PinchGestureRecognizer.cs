﻿namespace Slash.Unity.GestureInput.Gestures.Pinch
{
    using System;
    using System.Collections.Generic;
    using Slash.Unity.GestureInput.Modules;
    using UnityEngine;

    [Serializable]
    public class PinchGestureRecognizer : GestureRecognizer<PinchEventData>
    {
        /// <summary>
        ///     Factor to convert mouse scroll to pinch distance (in mm).
        /// </summary>
        public float MouseScrollToDistanceFactor = 1;

        /// <inheritdoc />
        public override IEnumerable<RegisteredInputSource> GetInputSources()
        {
            yield return new RegisteredInputSource<PinchSource, PinchEventData>(
                (source, data) => source.OnPinch(data));
        }

        /// <inheritdoc />
        public override void Process()
        {
            if (Input.touches.Length == 2)
            {
                var touch1 = Input.touches[0];
                var touch2 = Input.touches[1];

                if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
                {
                    // ... check the delta distance between them ...
                    var pinchDistance = Vector2.Distance(touch1.position, touch2.position);
                    var prevDistance = Vector2.Distance(touch1.position - touch1.deltaPosition,
                        touch2.position - touch2.deltaPosition);
                    var deltaMove = pinchDistance - prevDistance;

                    // Convert to mm.
                    deltaMove = deltaMove * 25.4f / Screen.dpi;

                    OnGestureDetected(new PinchEventData {DeltaMove = deltaMove},
                        (touch1.position + touch2.position) * 0.5f);
                }
            }
            else if (Math.Abs(Input.mouseScrollDelta.y) > 0.001f)
            {
                OnGestureDetected(
                    new PinchEventData {DeltaMove = Input.mouseScrollDelta.y * MouseScrollToDistanceFactor},
                    Input.mousePosition);
            }
        }
    }
}