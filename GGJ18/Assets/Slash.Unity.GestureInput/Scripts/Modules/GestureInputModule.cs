﻿namespace Slash.Unity.GestureInput.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Slash.Unity.GestureInput.Devices;
    using Slash.Unity.GestureInput.Gestures;
    using Slash.Unity.GestureInput.Raycasts;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class GestureInputModule : PointerInputModule
    {
        /// <summary>
        ///     Indicates if detected gestures are logged to console.
        /// </summary>
        public bool LogGestures;

        private List<GestureRecognizer> gestureRecognizers;

        private readonly List<GameObject> pointedGameObjects = new List<GameObject>();

        private PointerEventData pointerEventData;

        private PointerTouchDetection pointerTouchDetection;

        private readonly Dictionary<Type, List<RegisteredInputSource>> registeredInputSources =
            new Dictionary<Type, List<RegisteredInputSource>>();

        public IPointerDevice WorldPointer { get; set; }

        protected IPointerTouchDetection PointerTouchDetection
        {
            get
            {
                return this.pointerTouchDetection;
            }
        }

        public event Action<IGestureData, List<GameObject>> GestureDetected;

        public override void ActivateModule()
        {
            base.ActivateModule();

            this.pointerTouchDetection = new PointerTouchDetection(this.eventSystem);
        }

        public void AddRecognizer(GestureRecognizer gestureRecognizer)
        {
            gestureRecognizer.GestureDetected += this.OnGestureDetected;
            gestureRecognizer.GestureDetectedAtPosition += this.GestureDetectedAtPosition;
            gestureRecognizer.GestureDetectedForReceivers += this.OnGestureDetectedForReceivers;
            this.gestureRecognizers.Add(gestureRecognizer);

            foreach (var inputSource in gestureRecognizer.GetInputSources())
            {
                this.AddInputSource(inputSource);
            }
        }

        public override void Process()
        {
            this.UpdatePointerEventData();

            this.SendUpdateEventToSelectedObject();

            this.ProcessMove(this.pointerEventData);
            this.ProcessDrag(this.pointerEventData);

            foreach (var gestureRecognizer in this.gestureRecognizers)
            {
                gestureRecognizer.Process();
            }
        }

        public void SendGesture(IGestureData gestureData, List<GameObject> receivers)
        {
            if (this.LogGestures)
            {
                Debug.LogFormat(this, "Gesture detected: {0}", gestureData);
            }

            var eventType = gestureData.GetType();
            List<RegisteredInputSource> inputSources;
            if (!this.registeredInputSources.TryGetValue(eventType, out inputSources))
            {
                Debug.LogFormat("No registered input source found for input event '{0}'", eventType);
                return;
            }

            foreach (var receiver in receivers)
            {
                if (SendGesture(gestureData, receiver, inputSources))
                {
                    break;
                }
            }
        }
        
        private static bool SendGesture(IGestureData eventData, GameObject receiver, List<RegisteredInputSource> inputSources)
        {
            if (receiver == null)
            {
                return false;
            }

            var target = receiver.transform;
            var handled = false;
            while (target != null)
            {
                foreach (var inputSource in inputSources)
                {
                    if (inputSource.TryHandle(eventData, target.gameObject))
                    {
                        handled = true;
                        break;
                    }
                }

                if (handled)
                {
                    break;
                }

                target = target.parent;
            }

            return handled;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var debugText = string.Format("{0}\n\n", base.ToString());

            if (this.pointerEventData != null)
            {
                debugText += string.Format("Pointer: {0}\n\n", this.pointerEventData);
            }

            foreach (var gestureRecognizer in this.gestureRecognizers)
            {
                debugText += string.Format("Recognizer: {0}\n", gestureRecognizer.GetType().Name);
                debugText += string.Format("{0}\n\n", gestureRecognizer);
            }
            return debugText;
        }

        protected void AddInputSource<TSource, TEventData>(Func<TSource, TEventData, bool> eventHandler)
            where TSource : class, IEventSystemHandler where TEventData : IGestureData
        {
            AddInputSource(new RegisteredInputSource<TSource, TEventData>(eventHandler));
        }

        protected override void Awake()
        {
            base.Awake();

            this.gestureRecognizers = new List<GestureRecognizer>();
        }

        protected virtual bool SendUpdateEventToSelectedObject()
        {
            if (eventSystem.currentSelectedGameObject == null)
                return false;

            var data = GetBaseEventData();
            ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
            return data.used;
        }

        private void AddInputSource(RegisteredInputSource inputSource)
        {
            var eventDataType = inputSource.GetEventDataType();

            List<RegisteredInputSource> inputSources;
            if (!this.registeredInputSources.TryGetValue(eventDataType, out inputSources))
            {
                inputSources = new List<RegisteredInputSource>();
                this.registeredInputSources[eventDataType] = inputSources;
            }
            inputSources.Add(inputSource);
        }

        private void GestureDetectedAtPosition(IGestureData eventData, Vector2 position)
        {
            if (
                !this.pointerTouchDetection.GetTouchedGameObjects(
                    position,
                    this.pointedGameObjects))
            {
                return;
            }

            this.SendGesture(eventData, this.pointedGameObjects);

            var handler = this.GestureDetected;
            if (handler != null)
            {
                handler(eventData, this.pointedGameObjects);
            }
        }

        private void OnGestureDetected(IGestureData eventData)
        {
            if (this.WorldPointer == null)
            {
                return;
            }
            this.GestureDetectedAtPosition(eventData, this.WorldPointer.GetPosition());
        }

        private void OnGestureDetectedForReceivers(IGestureData gestureData, IEnumerable<GameObject> receivers)
        {
            var receiversList = receivers.ToList();
            this.SendGesture(gestureData, receiversList);

            var handler = this.GestureDetected;
            if (handler != null)
            {
                handler(gestureData, receiversList);
            }
        }

        private void UpdatePointerEventData()
        {
            if (this.pointerEventData == null)
            {
                this.pointerEventData = new PointerEventData(this.eventSystem);
            }

            if (this.WorldPointer != null)
            {
                this.pointerEventData.position = this.WorldPointer.GetPosition();
                var raycastResults = new List<RaycastResult>();
                this.eventSystem.RaycastAll(this.pointerEventData, raycastResults);
                this.pointerEventData.pointerCurrentRaycast = FindFirstRaycast(raycastResults);
            }
        }
    }
}