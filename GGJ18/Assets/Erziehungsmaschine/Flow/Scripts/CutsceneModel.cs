﻿using System;

namespace Erziehungsmaschine.Flow
{
    public class CutsceneModel
    {
        /// <summary>
        ///     Current running cutscene.
        /// </summary>
        public Cutscene Cutscene;

        /// <summary>
        ///     Callback to invoke when cutscene is finished.
        /// </summary>
        public Action CutsceneFinishedCallback;

        /// <summary>
        ///     Current step in running cutscene.
        /// </summary>
        public int CutsceneStepIndex;

        public void OnCutsceneFinished()
        {
            var handler = CutsceneFinishedCallback;
            if (handler != null)
            {
                handler();
            }
        }
    }
}