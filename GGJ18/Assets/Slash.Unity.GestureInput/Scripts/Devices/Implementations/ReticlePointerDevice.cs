﻿using System;
using UnityEngine;

namespace Slash.Unity.GestureInput.Devices.Implementations
{
    [Serializable]
    public class ReticlePointerDevice : IPointerDevice
    {
        /// <summary>
        ///   Camera of the reticle.
        /// </summary>
        public Camera Camera;
        
        private Vector2 position;

        /// <inheritdoc />
        public Vector2 GetPosition()
        {
            return this.position;
        }

        /// <inheritdoc />
        public bool IsDown()
        {
            return false;
        }

        public void Init()
        {
            this.position = this.Camera != null
                ? (Vector2) this.Camera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f))
                : Vector2.zero;
        }
    }
}