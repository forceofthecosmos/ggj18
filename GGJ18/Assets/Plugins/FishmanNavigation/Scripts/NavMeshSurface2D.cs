﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[ExecuteInEditMode]
[DefaultExecutionOrder(-102)]
public class NavMeshSurface2D : NavMeshSurface
{
    private void Awake()
    {
        transform.localEulerAngles = new Vector3(-90,0,0);
    }

    [SerializeField]
    private PolygonCollider2D[] polygonColliders;

    protected override List<NavMeshBuildSource> CollectSources()
    {
        List<NavMeshBuildSource> sources = new List<NavMeshBuildSource>();
        var modifiers = NavMeshModifier.activeModifiers;
        for (int i = 0; i < polygonColliders.Length; i++)
        {
            var polygonCollider = polygonColliders[i];

            var buildSource = new NavMeshBuildSource();
            buildSource.shape = NavMeshBuildSourceShape.Mesh;
            buildSource.size = new Vector3(1,1,1);
            buildSource.transform = Matrix4x4.identity;
            buildSource.component = polygonCollider.transform;
            buildSource.area = defaultArea;

            Triangulator tria = new Triangulator(polygonCollider.points);
            var indicies = tria.Triangulate();
            var vertices2D = polygonCollider.points;

            // Create the Vector3 vertices
            Vector3[] vertices = new Vector3[vertices2D.Length];
            for (int j=0; j<vertices.Length; j++) {
                vertices[j] = new Vector3(
                    vertices2D[j].x + polygonCollider.transform.position.x,
                    vertices2D[j].y + polygonCollider.transform.position.y,
                    0);
            }

            // Create the mesh
            Mesh msh = new Mesh();
            msh.vertices = vertices;
            msh.triangles = indicies;
            msh.RecalculateNormals();
            msh.RecalculateBounds();

            buildSource.sourceObject = msh;

            for (int j = 0; j < modifiers.Count; j++)
            {
                var modifier = modifiers[j];

                if (modifier.gameObject != polygonCollider.gameObject)
                {
                    continue;
                }

                if (modifier.overrideArea)
                {
                    buildSource.area = modifier.area;
                }
            }

            sources.Add(buildSource);
        }


        AppendModifierVolumes(ref sources);
        return sources;
    }
}
