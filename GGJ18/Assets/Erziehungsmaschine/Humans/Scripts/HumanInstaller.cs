﻿using Slash.Unity.GestureInput.Sources;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Humans
{
    public class HumanInstaller : MonoInstaller
    {
        public Animator Animator;

        public bool CreateDummyModel;

        public DragSource DragSource;

        public override void InstallBindings()
        {
            if (CreateDummyModel)
            {
                Container.BindInstance(new HumanModel {DebugName = "Dummy Human"});
            }

            Container.Bind<HumanController>().AsSingle();

            Container.BindInstance(Animator);
            Container.BindInterfacesAndSelfTo<HumanAnimator>().AsSingle();

            Container.BindInstance(DragSource);
            Container.BindInterfacesAndSelfTo<HumanDragHandler>().AsSingle();
        }
    }
}