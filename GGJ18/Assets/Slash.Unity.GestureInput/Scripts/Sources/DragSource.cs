﻿namespace Slash.Unity.GestureInput.Sources
{
    using System;
    using Slash.Unity.GestureInput.Gestures.Implementations;

    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class DragSource : MonoBehaviour, IEventSystemHandler
    {
        public BeginDragEvent BeginDrag = new BeginDragEvent();

        public DragEvent Drag = new DragEvent();

        public EndDragEvent EndDrag = new EndDragEvent();

        public bool OnBeginDrag(BeginDragEventData data)
        {
            this.BeginDrag.Invoke(data);
            return true;
        }

        public bool OnDrag(DragEventData eventData)
        {
            this.Drag.Invoke(eventData);
            return true;
        }

        public bool OnEndDrag(EndDragEventData eventData)
        {
            this.EndDrag.Invoke(eventData);
            return true;
        }

        [Serializable]
        public class BeginDragEvent : UnityEvent<BeginDragEventData>
        {
        }

        [Serializable]
        public class DragEvent : UnityEvent<DragEventData>
        {
        }

        [Serializable]
        public class EndDragEvent : UnityEvent<EndDragEventData>
        {
        }
    }
}