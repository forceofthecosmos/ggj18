﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Erziehungsmaschine.Constructions
{
    public class ProgressIndicatorView : MonoBehaviour
    {
        public Image ProgressFill;

        private ConstructionModel model;

        [Inject]
        public void Initialize(ConstructionModel processInject)
        {
            model = processInject;
        }

        protected void Update()
        {
            if (ProgressFill != null)
            {
                var conversionProcess = model.ConversionProcess;
                ProgressFill.fillAmount = conversionProcess != null ? conversionProcess.ProgressRatio : 1.0f;
            }
        }
    }
}