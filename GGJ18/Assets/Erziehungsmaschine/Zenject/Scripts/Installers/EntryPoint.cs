﻿using System.Collections;
using Erziehungsmaschine.Humans;
using Erziehungsmaschine.Level;
using Erziehungsmaschine.Menu;
using Erziehungsmaschine.Sounds.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Erziehungsmaschine.Zenject.Installers
{
    public class EntryPoint : MonoBehaviour
    {
        public float DelayTillStart = 0.5f;

        /// <summary>
        ///     Scenes that contain the game content.
        /// </summary>
        public string[] Scenes;

        [SerializeField] private EntryMenuFade entryMenuFade;

        private HumansController humansController;

        [SerializeField] private bool isSkippingMenuFade;


        [SerializeField] private LevelConfigScriptableObject levelConfig;

        private LevelController levelController;

        private SoundController soundController;

        private SoundSettingsScriptableObject.AudioSettings audioSettings;

        [Inject]
        public void Initialize(
            HumansController humansController,
            LevelController levelController,
            SoundController soundController,
            SoundSettingsScriptableObject.AudioSettings audioSettings)
        {
            this.audioSettings = audioSettings;
            this.soundController = soundController;
            this.levelController = levelController;
            this.humansController = humansController;
        }

        public void StartGame()
        {
            StartCoroutine(StartGameAfterDelay(DelayTillStart));
        }

        private void LoadLevel(LevelConfigScriptableObject config)
        {
            if (config == null)
            {
                Debug.LogError("No start level defined", this);
                return;
            }

            levelController.LoadLevel(config);
        }

        private void Start()
        {
            soundController.PlayAtmoClip(false);

            foreach (var scene in Scenes)
            {
                SceneManager.LoadScene(scene, LoadSceneMode.Additive);
            }

            if (isSkippingMenuFade)
            {
                entryMenuFade.FinishSequence();
            }
        }

        private IEnumerator StartGameAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);

            soundController.PlayAtmoClip(true);
            LoadLevel(levelConfig);
            soundController.PlayClip(audioSettings.GetReady);
            humansController.StartGame();
        }
    }
}