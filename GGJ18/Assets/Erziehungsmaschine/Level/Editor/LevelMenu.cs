﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Erziehungsmaschine.Level.Editor
{
    public static class LevelMenu
    {
#if UNITY_EDITOR
        [MenuItem("Tools/Create Level Config")]
        public static void Create()
        {
            var obj = ScriptableObject.CreateInstance<LevelConfigScriptableObject>();
            var selectedPath = AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[0]);
            if (string.IsNullOrEmpty(selectedPath))
            {
                Debug.LogError("Select an directory");
                return;
            }

            var assetPath = Path.Combine(selectedPath, "LevelConfig.asset");
            AssetDatabase.CreateAsset(obj, assetPath);
        }
#endif
    }
}