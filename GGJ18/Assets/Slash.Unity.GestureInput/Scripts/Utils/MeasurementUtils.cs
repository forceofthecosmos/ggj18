﻿namespace Slash.Unity.GestureInput.Utils
{
    using UnityEngine;

    public static class MeasurementUtils
    {
        /// <summary>
        ///   Number of pixels per millimeter.
        /// </summary>
        public static float PixelsPerMillimeter
        {
            get { return Screen.dpi / 25.4f; }
        }

        public static float ConvertToMillimeter(float pixels)
        {
            return pixels / PixelsPerMillimeter;
        }

        public static float ConvertToPixels(float millimeters)
        {
            return millimeters * PixelsPerMillimeter;
        }
    }
}