﻿namespace Slash.Unity.GestureInput.Gestures.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Slash.Unity.GestureInput.Modules;
    using Slash.Unity.GestureInput.Raycasts;
    using Slash.Unity.GestureInput.Sources;
    using Slash.Unity.GestureInput.Utils;
    using UnityEngine;
    using UnityEngine.EventSystems;

    [Flags]
    public enum SwipeDirection
    {
        None = 0,

        Up = 1 << 1,

        Down = 1 << 2,

        Left = 1 << 3,

        Right = 1 << 4
    }

    public class SwipeEventData : BaseEventData, IGestureData
    {
        public SwipeEventData(EventSystem eventSystem)
            : base(eventSystem)
        {
        }

        public SwipeDirection Direction { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Format("Direction: {0}", this.Direction);
        }
    }

    [Serializable]
    public class SwipeGestureRecognizer : GestureRecognizer<SwipeEventData>
    {
        /// <summary>
        ///     Game objects which were touched when swipe started.
        /// </summary>
        private readonly List<GameObject> touchedGameObjects = new List<GameObject>();

        /// <summary>
        ///     Indicates if details about recognized swipes should be logged.
        /// </summary>
        public bool LogSwipes;

        /// <summary>
        ///     Minimum distance to move pointer to count as swipe (in mm).
        /// </summary>
        [SerializeField]
        [Tooltip("Minimum distance to move pointer to count as swipe (in mm)")]
        protected float MinSwipeDistance = 0.3f;

        /// <summary>
        ///     Minimum speed to move pointer to count as swipe (in mm/s).
        /// </summary>
        [SerializeField]
        [Tooltip("Minimum speed to move pointer to count as swipe (in mm/s)")]
        protected float MinSwipeSpeed = 0.5f;

        /// <summary>
        ///     Position where pointer was pressed (in pixels).
        /// </summary>
        private Vector2 pointerDownPosition;

        /// <summary>
        ///     Time when the pointer was pressed.
        /// </summary>
        private float pointerDownTime;

        private IPointerTouchDetection pointerTouchDetection;

        /// <summary>
        ///     Position where pointer was released (in pixels).
        /// </summary>
        private Vector2 pointerUpPosition;

        /// <summary>
        ///     Time when the pointer was released.
        /// </summary>
        private float pointerUpTime;

        [SerializeField]
        protected string SimulateSwipeDownButton;

        [SerializeField]
        protected string SimulateSwipeLeftButton;

        [SerializeField]
        protected string SimulateSwipeRightButton;

        [SerializeField]
        protected string SimulateSwipeUpButton;

        public override IEnumerable<RegisteredInputSource> GetInputSources()
        {
            yield return new RegisteredInputSource<SwipeSource, SwipeEventData>((source, data) => source.OnSwipe(data));
        }

        public void Init(IPointerTouchDetection touchDetection)
        {
            this.pointerTouchDetection = touchDetection;
        }

        public override void Process()
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.OnPointerDown(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                this.OnPointerUp(Input.mousePosition);
            }

            // Process simulation.
            if (!string.IsNullOrEmpty(this.SimulateSwipeLeftButton) &&
                Input.GetButtonDown(this.SimulateSwipeLeftButton))
            {
                this.OnSwipe(SwipeDirection.Left);
            }

            if (!string.IsNullOrEmpty(this.SimulateSwipeRightButton)
                && Input.GetButtonDown(this.SimulateSwipeRightButton))
            {
                this.OnSwipe(SwipeDirection.Right);
            }

            if (!string.IsNullOrEmpty(this.SimulateSwipeUpButton) && Input.GetButtonDown(this.SimulateSwipeUpButton))
            {
                this.OnSwipe(SwipeDirection.Up);
            }

            if (!string.IsNullOrEmpty(this.SimulateSwipeDownButton) &&
                Input.GetButtonDown(this.SimulateSwipeDownButton))
            {
                this.OnSwipe(SwipeDirection.Down);
            }
        }

        private SwipeDirection DetectSwipe()
        {
            var swipeMovement = this.pointerUpPosition - this.pointerDownPosition;

            // Convert to millimeter.
            if (Screen.dpi > 0)
            {
                // Pixels to inch.
                swipeMovement /= Screen.dpi;

                // Inch to millimeter.
                swipeMovement *= 25.4f;
            }

            var swipeDuration = this.pointerUpTime - this.pointerDownTime;
            var swipeDistance = swipeMovement.magnitude;

            // Check speed.
            var swipeSpeed = swipeDistance / swipeDuration;
            if (swipeSpeed < this.MinSwipeSpeed)
            {
                if (this.LogSwipes)
                {
                    Debug.LogFormat("Swipe ignored due to too slow movement (speed: {0}mm/s)", swipeSpeed);
                }

                return SwipeDirection.None;
            }

            // Get the direction from the mouse position when pointer is pressed to when it is released.
            var swipeDistanceVertical = Mathf.Abs(swipeMovement.y);
            var swipeDistanceHorizontal = Mathf.Abs(swipeMovement.x);

            if (swipeDistanceHorizontal < this.MinSwipeDistance &&
                swipeDistanceVertical < this.MinSwipeDistance)
            {
                if (this.LogSwipes)
                {
                    Debug.LogFormat("Swipe ignored due to too little distance (vertical: {0}mm, horizontal: {1}mm)",
                        swipeDistanceVertical,
                        swipeDistanceHorizontal);
                }

                return SwipeDirection.None;
            }

            var swipeIsVertical = swipeDistanceVertical > swipeDistanceHorizontal;
            var swipeIsHorizontal = !swipeIsVertical;

            // If the swipe has a positive y component and is vertical the swipe is up.
            if (swipeMovement.y > 0f && swipeIsVertical)
            {
                if (this.LogSwipes)
                {
                    Debug.LogFormat("Swipe Up detected (distance: {0}mm, duration: {1}s)", swipeDistanceVertical,
                        swipeDuration);
                }

                return SwipeDirection.Up;
            }

            // If the swipe has a negative y component and is vertical the swipe is down.
            if (swipeMovement.y < 0f && swipeIsVertical)
            {
                if (this.LogSwipes)
                {
                    Debug.LogFormat("Swipe Down detected (distance: {0}mm, duration: {1}s)", swipeDistanceVertical,
                        swipeDuration);
                }

                return SwipeDirection.Down;
            }

            // If the swipe has a positive x component and is horizontal the swipe is right.
            if (swipeMovement.x > 0f && swipeIsHorizontal)
            {
                if (this.LogSwipes)
                {
                    Debug.LogFormat("Swipe Right detected (distance: {0}mm, duration: {1}s)", swipeDistanceHorizontal,
                        swipeDuration);
                }

                return SwipeDirection.Right;
            }

            // If the swipe has a negative x component and is vertical the swipe is left.
            if (swipeMovement.x < 0f && swipeIsHorizontal)
            {
                if (this.LogSwipes)
                {
                    Debug.LogFormat("Swipe Left detected (distance: {0}mm, duration: {1}s)", swipeDistanceHorizontal,
                        swipeDuration);
                }

                return SwipeDirection.Left;
            }

            // If the swipe meets none of these requirements there is no swipe.
            return SwipeDirection.None;
        }

        private void OnPointerDown(Vector2 position)
        {
            this.pointerDownPosition = position;
            this.pointerDownTime = Time.time;

            // Store touched game objects.
            this.pointerTouchDetection.GetTouchedGameObjects(position, this.touchedGameObjects);
        }

        private void OnPointerUp(Vector2 position)
        {
            this.pointerUpPosition = position;
            this.pointerUpTime = Time.time;

            var swipeDirection = this.DetectSwipe();
            if (swipeDirection != SwipeDirection.None)
            {
                this.OnSwipe(swipeDirection);
            }
        }

        private void OnSwipe(SwipeDirection swipeDirection)
        {
            if (this.LogSwipes)
            {
                Debug.LogFormat("Swipe detected: {0} on game objects {1}", swipeDirection,
                    this.touchedGameObjects.Select(touchedGameObject => touchedGameObject.name).Implode(";"));
            }

            var swipeEventData = new SwipeEventData(null) {Direction = swipeDirection};
            this.OnGestureDetected(swipeEventData, this.touchedGameObjects);
        }
    }
}