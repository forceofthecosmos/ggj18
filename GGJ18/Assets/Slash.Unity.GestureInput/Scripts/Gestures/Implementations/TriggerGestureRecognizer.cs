﻿namespace Slash.Unity.GestureInput.Gestures.Implementations
{
    using System;
    using System.Collections.Generic;
    using Slash.Unity.GestureInput.Devices;
    using Slash.Unity.GestureInput.Modules;
    using Slash.Unity.GestureInput.Raycasts;
    using Slash.Unity.GestureInput.Sources;
    using Slash.Unity.GestureInput.Utils;
    using UnityEngine;
    using UnityEngine.EventSystems;

    [Serializable]
    public class TriggerEventData : BaseEventData, IGestureData
    {
        /// <summary>
        ///     Button which was triggered.
        /// </summary>
        public string Button;

        /// <summary>
        ///     Constructor.
        /// </summary>
        /// <param name="eventSystem">Event system the event data belongs to.</param>
        public TriggerEventData(EventSystem eventSystem) : base(eventSystem)
        {
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Format("Button: {0}", this.Button);
        }
    }

    [Serializable]
    public class TriggerGestureRecognizer : GestureRecognizer<TriggerEventData>
    {
        /// <summary>
        ///     Indicates if details about triggers should be logged.
        /// </summary>
        public bool IsLoggingEnabled;

        /// <summary>
        ///     Trigger which should be used to simulate pointer clicks e.g. on buttons.
        /// </summary>
        [Tooltip("Trigger which should be used to simulate pointer clicks e.g. on buttons")]
        public string PointerClickTrigger = "Tap";

        private IPointerDevice pointerDevice;

        private IPointerTouchDetection pointerTouchDetection;

        /// <summary>
        ///     Named trigger buttons.
        /// </summary>
        public List<Trigger> Triggers = new List<Trigger> {new Trigger {Name = "Submit", TriggerButton = "Submit"}};

        public override IEnumerable<RegisteredInputSource> GetInputSources()
        {
            yield return
                new RegisteredInputSource<TriggerSource, TriggerEventData>((source, data) => source.OnTrigger(data));
            if (!string.IsNullOrEmpty(this.PointerClickTrigger))
            {
                yield return
                    new RegisteredInputSource<IPointerClickHandler, TriggerEventData>((source, data) =>
                    {
                        if (data.Button != this.PointerClickTrigger)
                        {
                            return false;
                        }

                        var pointerEventData = new PointerEventData(null) {button = PointerEventData.InputButton.Left};
                        source.OnPointerClick(pointerEventData);
                        return true;
                    });
            }
        }

        public void Init(IPointerDevice pointer, IPointerTouchDetection touchDetection)
        {
            this.pointerDevice = pointer;
            this.pointerTouchDetection = touchDetection;
        }

        /// <inheritdoc />
        public override void Process()
        {
            // Check if initialized.
            if (this.pointerDevice == null || this.pointerTouchDetection == null)
            {
                Debug.LogError("TriggerGestureRecognizer not correctly initialized");
                return;
            }

            // Process triggers.
            foreach (var trigger in this.Triggers)
            {
                this.ProcessTrigger(trigger);
            }
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Format("MoveThreshold: \n\nTriggers:\n{0}", this.Triggers.Implode("\n\n"));
        }

        private void OnGestureDetected(Trigger trigger)
        {
            this.OnGestureDetected(new TriggerEventData(null) {Button = trigger.Name});
        }

        private void ProcessTrigger(Trigger trigger)
        {
            if (Input.GetButtonDown(trigger.TriggerButton))
            {
                if (trigger.Mode == Trigger.TriggerMode.Push)
                {
                    this.OnGestureDetected(trigger);
                }
                else
                {
                    trigger.PressPosition = trigger.LastPosition = this.pointerDevice.GetPosition();
                    trigger.PressTime = Time.time;
                    trigger.PressedGameObject = this.pointerTouchDetection.GetTouchedGameObject(trigger.PressPosition);
                    trigger.SqrMoveDistance = 0;

                    if (this.IsLoggingEnabled)
                    {
                        Debug.LogFormat("Trigger {0}: Pressed at position {1} (time: {2}) on {3}", trigger.Name,
                            trigger.PressPosition,
                            trigger.PressTime,
                            trigger.PressedGameObject != null ? trigger.PressedGameObject.name : null);
                    }
                }
            }

            if (trigger.PressedGameObject != null)
            {
                // Accumulate move distance.
                var position = this.pointerDevice.GetPosition();
                var movement = position - trigger.LastPosition;
                trigger.SqrMoveDistance += movement.sqrMagnitude;
                trigger.LastPosition = position;
            }

            if (Input.GetButtonUp(trigger.TriggerButton))
            {
                if (trigger.Mode == Trigger.TriggerMode.Release && this.ShouldTrigger(trigger))
                {
                    this.OnGestureDetected(trigger);
                }

                trigger.PressedGameObject = null;
            }
        }

        private bool ShouldTrigger(Trigger trigger)
        {
            // Check if button was released on pressed game object.
            var pointerPosition = this.pointerDevice.GetPosition();
            var touchedGameObject = this.pointerTouchDetection.GetTouchedGameObject(pointerPosition);
            if (touchedGameObject != trigger.PressedGameObject)
            {
                if (this.IsLoggingEnabled)
                {
                    Debug.LogFormat(
                        "Trigger {0} for {1}: Not executing because pointer touches different trigger source ({2}) on release",
                        trigger.Name,
                        trigger.PressedGameObject != null ? trigger.PressedGameObject.name : "<null>",
                        touchedGameObject != null ? touchedGameObject.name : "<null>");
                }

                return false;
            }

            // Check if moved too much.
            var deltaMove = Math.Sqrt(trigger.SqrMoveDistance);

            // Convert to millimeter.
            if (Screen.dpi > 0)
            {
                // Pixels to inch.
                deltaMove /= Screen.dpi;

                // Incho to millimeter.
                deltaMove *= 25.4f;
            }

            if (deltaMove > trigger.MoveThreshold)
            {
                if (this.IsLoggingEnabled)
                {
                    Debug.LogFormat(
                        "Trigger {0}: Canceled due to too much movement: {1}mm (dpi: {2}, threshold: {3}mm)",
                        trigger.Name,
                        deltaMove,
                        Screen.dpi,
                        trigger.MoveThreshold);
                }

                return false;
            }

            // Check if pressed for the correct duration.
            var pressDuration = Time.time - trigger.PressTime;
            if (pressDuration < trigger.PressDurationMin ||
                pressDuration > trigger.PressDurationMax)
            {
                if (this.IsLoggingEnabled)
                {
                    Debug.LogFormat(
                        "Trigger {0}: Canceled due to too short or too long duration: {1}s (min: {2}s, max: {3}s)",
                        trigger.Name,
                        pressDuration,
                        trigger.PressDurationMin,
                        trigger.PressDurationMax);
                }

                return false;
            }

            return true;
        }

        [Serializable]
        public class Trigger
        {
            public enum TriggerMode
            {
                Release,

                Push
            }

            /// <summary>
            ///     Defines when trigger is fired.
            /// </summary>
            [Tooltip("Defines when trigger is fired")]
            public TriggerMode Mode;

            /// <summary>
            ///     Threshold the pointer is allowed to move without canceling the trigger (in mm).
            /// </summary>
            [Tooltip("Threshold the pointer is allowed to move without canceling the trigger (in mm)")]
            public double MoveThreshold = 2.0f;

            /// <summary>
            ///     Name of trigger. Some sources may be only valid for specific triggers.
            /// </summary>
            [Tooltip("Name of trigger. Some sources may be only valid for specific triggers")]
            public string Name = string.Empty;

            /// <summary>
            ///     Maximum duration the trigger is allowed to be pressed so the gesture is detected (in s).
            ///     Only considered in <see cref="TriggerMode" />.Release.
            /// </summary>
            [HideInInspector]
            [Tooltip("Maximum duration the trigger is allowed to be pressed so the gesture is detected (in s)")]
            public float PressDurationMax = float.MaxValue;

            /// <summary>
            ///     Minimum duration the trigger needs to be pressed so the gesture is detected (in s).
            ///     Only considered in <see cref="TriggerMode" />.Release.
            /// </summary>
            [HideInInspector]
            [Tooltip("Minimum duration the trigger needs to be pressed so the gesture is detected (in s)")]
            public float PressDurationMin = 0.0f;

            /// <summary>
            ///     Name of input which triggers the gesture (from Input settings).
            /// </summary>
            [Tooltip("Button which triggers the gesture (from Input settings)")]
            public string TriggerButton = "Submit";

            /// <summary>
            ///     Position where pointer was at last frame (in pixels).
            /// </summary>
            public Vector2 LastPosition { get; set; }

            /// <summary>
            ///     Game object the pointer was pressed on.
            /// </summary>
            public GameObject PressedGameObject { get; set; }

            /// <summary>
            ///     Position where the pointer was pressed (in pixels).
            /// </summary>
            public Vector2 PressPosition { get; set; }

            /// <summary>
            ///     Time the trigger button was pressed.
            /// </summary>
            public float PressTime { get; set; }

            /// <summary>
            ///     Squared distance the pointer moved during press (in pixels).
            /// </summary>
            public float SqrMoveDistance { get; set; }

            public override string ToString()
            {
                return
                    string.Format(
                        "Mode: {0}, Name: {1}, TriggerButton: {2}, PressedGameObject: {3}, PressPosition: {4}",
                        this.Mode,
                        this.Name,
                        this.TriggerButton,
                        this.PressedGameObject,
                        this.PressPosition);
            }
        }
    }
}