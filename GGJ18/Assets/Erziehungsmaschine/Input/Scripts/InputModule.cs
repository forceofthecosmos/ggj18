﻿using Slash.Unity.GestureInput.Devices.Implementations;
using Slash.Unity.GestureInput.Gestures.Implementations;
using Slash.Unity.GestureInput.Modules;

namespace Erziehungsmaschine.Input
{
    public class InputModule : GestureInputModule
    {
        public DragGestureRecognizer DragGestureRecognizer;

        public MousePointerDevice MousePointerDevice;

        public TriggerGestureRecognizer TriggerGestureRecognizer;

        public override void ActivateModule()
        {
            base.ActivateModule();

            WorldPointer = MousePointerDevice;

            TriggerGestureRecognizer.Init(WorldPointer, PointerTouchDetection);
            AddRecognizer(TriggerGestureRecognizer);

            DragGestureRecognizer.Init(WorldPointer, PointerTouchDetection);
            AddRecognizer(DragGestureRecognizer);
        }
    }
}