﻿using UnityEngine;

namespace Slash.Unity.GestureInput.Devices
{
    public interface IPointerDevice
    {
        /// <summary>
        ///     Current screen position of the pointer.
        /// </summary>
        /// <returns>Returns the current screen position of the pointer.</returns>
        Vector2 GetPosition();

        /// <summary>
        ///     Indicates if the pointer is pressed.
        /// </summary>
        /// <returns>True if the pointer is pressed; otherwise, false.</returns>
        bool IsDown();
    }
}