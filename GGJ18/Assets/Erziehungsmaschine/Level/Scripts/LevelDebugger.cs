﻿using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Level
{
    public class LevelDebugger : MonoBehaviour
    {
        private LevelController controller;

        [ContextMenu("Add correct human")]
        public void AddCorrectHuman()
        {
            controller.AddCorrectHuman();
        }

        [ContextMenu("Add wrong human")]
        public void AddWrongHuman()
        {
            controller.AddWrongHuman();
        }

        [Inject]
        public void Initialize(LevelController controllerInject)
        {
            controller = controllerInject;
        }

        [ContextMenu("Win level")]
        public void WinLevel()
        {
            controller.WinLevel();
        }

        [ContextMenu("Win game")]
        public void WinGame()
        {
            controller.GameOver(true);
        }

        [ContextMenu("Loose game")]
        public void LooseGame()
        {
            controller.GameOver(false);
        }
    }
}