﻿using Erziehungsmaschine.Humans;

namespace Erziehungsmaschine.Conversions
{
    public class ChangeFaceAttributeConversion : ChangeAttributeConversion<HumanFaceAttribute>
    {
        public ChangeFaceAttributeConversion() : base((attributes, value) => attributes.FaceAttribute = value,
            attributes => attributes.FaceAttribute)
        {
        }

        protected override HumanFaceAttribute Wildcard
        {
            get { return HumanFaceAttribute.Any; }
        }
    }
}