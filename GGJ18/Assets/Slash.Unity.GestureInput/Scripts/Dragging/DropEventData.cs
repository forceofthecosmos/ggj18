﻿using System.Collections.Generic;
using Slash.Unity.GestureInput.Gestures;
using Slash.Unity.GestureInput.Utils;
using UnityEngine;

namespace Slash.Unity.GestureInput.Dragging
{
    public class DropEventData : IGestureData
    {
        public List<GameObject> DragObjects { get; set; }

        public override string ToString()
        {
            return string.Format("DragObjects: {0}", DragObjects.Implode(";"));
        }
    }
}