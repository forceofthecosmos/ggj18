﻿namespace Slash.Unity.GestureInput.Utils
{
    using System;

    using UnityEngine;

    public static class UnityGameObjectUtility
    {
        /// <summary>
        ///   Returns the full path (i.e. names of all ancestors and self) to the game object.
        /// </summary>
        /// <param name="gameObject">Game object to get path for.</param>
        /// <returns>Full path of the specified game object.</returns>
        public static string GetPath(this GameObject gameObject)
        {
            string path = String.Empty;
            if (gameObject.transform.parent != null)
            {
                path = gameObject.transform.parent.gameObject.GetPath() + "/";
            }
            path += gameObject.name;
            return path;
        }
    }
}