﻿namespace Slash.Unity.GestureInput.Sources
{
    using System;

    using Slash.Unity.GestureInput.Gestures.Implementations;

    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class FocusSource : MonoBehaviour, IEventSystemHandler
    {
        public FocusGainedEvent FocusGained = new FocusGainedEvent();

        public FocusLostEvent FocusLost = new FocusLostEvent();

        public bool OnFocusGained(FocusGainedEventData eventData)
        {
            this.FocusGained.Invoke(this.gameObject);
            return true;
        }

        public bool OnFocusLost(FocusLostEventData eventData)
        {
            this.FocusLost.Invoke(this.gameObject);
            return true;
        }

        [Serializable]
        public class FocusGainedEvent : UnityEvent<GameObject>
        {
        }

        [Serializable]
        public class FocusLostEvent : UnityEvent<GameObject>
        {
        }
    }
}