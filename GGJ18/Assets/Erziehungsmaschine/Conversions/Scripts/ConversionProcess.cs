﻿using System;
using Erziehungsmaschine.Humans;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Conversions
{
    /// <summary>
    ///     A running conversion process.
    /// </summary>
    public class ConversionProcess : IFixedTickable
    {
        private readonly Conversion conversion;

        private readonly float duration;

        private readonly HumanModel human;

        private readonly Action onFinished;

        private float remainingDuration;

        public ConversionProcess(HumanModel human, float duration, Action onFinished, Conversion conversion)
        {
            if (duration <= 0)
            {
                throw new ArgumentException("Invalid duration", "duration");
            }

            this.human = human;
            this.duration = duration;
            this.onFinished = onFinished;
            this.conversion = conversion;
            remainingDuration = duration;
        }

        /// <summary>
        ///     Human which is converted.
        /// </summary>
        public HumanModel Human
        {
            get { return human; }
        }

        /// <summary>
        ///     When conversion is in progress, this ratio shows how much it is done (from 0-1).
        /// </summary>
        public float ProgressRatio
        {
            get { return 1.0f - remainingDuration / duration; }
        }

        public void FixedTick()
        {
            if (remainingDuration <= 0)
            {
                return;
            }

            remainingDuration -= Time.fixedDeltaTime;
            if (remainingDuration <= 0)
            {
                // Adjust human attributes.
                Debug.LogFormat("Doing conversion of human '{0}': '{1}'", human.DebugName, conversion);
                conversion.Execute(human.Attributes);

                OnFinished();
            }
        }

        public override string ToString()
        {
            return string.Format("Human: {0}, Duration: {1}, RemainingDuration: {2}", human, duration,
                remainingDuration);
        }

        protected virtual void OnFinished()
        {
            var handler = onFinished;
            if (handler != null)
            {
                handler();
            }
        }
    }
}