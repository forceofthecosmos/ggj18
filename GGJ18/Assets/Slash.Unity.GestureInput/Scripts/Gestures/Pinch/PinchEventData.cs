namespace Slash.Unity.GestureInput.Gestures.Pinch
{
    using System;

    [Serializable]
    public class PinchEventData : IGestureData
    {
        /// <summary>
        ///     Delta of distance between fingers (in mm).
        /// </summary>
        public float DeltaMove { get; set; }
    }
}