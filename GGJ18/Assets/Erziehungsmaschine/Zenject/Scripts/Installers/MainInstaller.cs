﻿using Erziehungsmaschine.Constructions.Signals;
using Erziehungsmaschine.Flow;
using Erziehungsmaschine.Misc;
using Zenject;

namespace Erziehungsmaschine.Zenject.Installers
{
    public class MainInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IInitializable>().To<CoroutineHelper>().AsSingle();
            Container.Bind<CoroutineHelper>().AsSingle();
            
            Container.DeclareSignal<UnlockConstructionSignal>();

            Container.DeclareSignal<TriggerCutsceneSignal>();
            Container.DeclareSignal<CutsceneStartedSignal>();
            Container.DeclareSignal<CutsceneFinishedSignal>();
        }
    }
}