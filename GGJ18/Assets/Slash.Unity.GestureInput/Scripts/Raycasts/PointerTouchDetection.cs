﻿namespace Slash.Unity.GestureInput.Raycasts
{
    using System.Collections.Generic;

    using UnityEngine;
    using UnityEngine.EventSystems;

    public class PointerTouchDetection : IPointerTouchDetection
    {
        private readonly EventSystem eventSystem;

        private readonly PointerEventData pointerEventData;

        private readonly List<RaycastResult> raycastResults = new List<RaycastResult>();

        public PointerTouchDetection(EventSystem eventSystem)
        {
            this.eventSystem = eventSystem;
            this.pointerEventData = new PointerEventData(eventSystem);
        }

        public GameObject GetTouchedGameObject(Vector2 screenPosition)
        {
            this.pointerEventData.position = screenPosition;
            this.eventSystem.RaycastAll(this.pointerEventData, this.raycastResults);
            var raycastResult = FindFirstRaycast(this.raycastResults);
            return raycastResult.gameObject;
        }

        public bool GetTouchedGameObjects(Vector2 screenPosition, List<GameObject> touchedGameObjects)
        {
            this.pointerEventData.position = screenPosition;
            this.eventSystem.RaycastAll(this.pointerEventData, this.raycastResults);

            touchedGameObjects.Clear();
            if (this.raycastResults.Count == 0)
            {
                return false;
            }

            for (var index = 0; index < this.raycastResults.Count; index++)
            {
                var raycastResult = this.raycastResults[index];
                if (raycastResult.gameObject != null)
                {
                    touchedGameObjects.Add(raycastResult.gameObject);
                }
            }
            return touchedGameObjects.Count > 0;
        }

        private static RaycastResult FindFirstRaycast(IEnumerable<RaycastResult> candidates)
        {
            foreach (var raycastResult in candidates)
            {
                if (raycastResult.gameObject == null)
                {
                    continue;
                }

                return raycastResult;
            }
            return new RaycastResult();
        }
    }
}