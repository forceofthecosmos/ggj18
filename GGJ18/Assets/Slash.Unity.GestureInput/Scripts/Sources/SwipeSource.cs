﻿namespace Slash.Unity.GestureInput.Sources
{
    using System;
    using Slash.Unity.GestureInput.Gestures.Implementations;
    using Slash.Unity.GestureInput.Utils;
    using UnityEngine;
    using UnityEngine.Events;

    public class SwipeSource : MonoBehaviour, ISwipeHandler
    {
        /// <summary>
        ///     Swipe directions to consider.
        /// </summary>
        [EnumFlags]
        public SwipeDirection DirectionMask =
            SwipeDirection.Down | SwipeDirection.Left | SwipeDirection.Right | SwipeDirection.Up;

        public SwipeEvent Swipe;

        public bool OnSwipe(SwipeEventData eventData)
        {
            // Check if correct direction.
            if (!this.DirectionMask.IsOptionSet(eventData.Direction))
            {
                return false;
            }

            this.Swipe.Invoke(eventData);
            return true;
        }

        [Serializable]
        public class SwipeEvent : UnityEvent<SwipeEventData>
        {
        }
    }
}