﻿namespace Slash.Unity.GestureInput.Recording
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    
    using Slash.Unity.GestureInput.Gestures;
    using Slash.Unity.GestureInput.Modules;
    using Slash.Unity.GestureInput.Utils;

    using UnityEngine;

    public class SessionPlayer : MonoBehaviour
    {
        public string FileName = "Session";

        public GestureInputModule InputModule;

        private Session loadedSession;

        private Transform playerTransform;

        private List<ScheduledGesture> scheduledGestures;

        private float startTime;

        [ContextMenu("Load Session")]
        public void LoadSession()
        {
            this.loadedSession = JsonUtility.FromJson<Session>(File.ReadAllText(this.FileName));
            this.PlaySession();
        }

        public void PlaySession(Session session)
        {
            this.startTime = Time.time;
            this.scheduledGestures = new List<ScheduledGesture>();
            foreach (var sessionGesture in session.Gestures)
            {
                var key = session.Keys.FirstOrDefault(sessionKey => sessionKey.Name == sessionGesture.Key);
                if (key == null)
                {
                    Debug.LogWarning("Taking start key as gesture has no key specified");
                    key = session.Keys[0];
                }

                var scheduledGesture = new ScheduledGesture
                {
                    Time = this.startTime + key.Time + sessionGesture.RelativeTime,
                    GestureData =
                        (IGestureData)
                            JsonUtility.FromJson(sessionGesture.Data, ReflectionUtils.FindType(sessionGesture.Type)),
                    Receivers = sessionGesture.Receivers,
                    PlayerOrientation = sessionGesture.PlayerOrientation
                };
                this.scheduledGestures.Add(scheduledGesture);
            }

            if (!string.IsNullOrEmpty(this.loadedSession.PlayerTransform))
            {
                var playerGameObject = GameObjectUtility.GetGameObject(this.loadedSession.PlayerTransform);
                this.playerTransform = playerGameObject != null ? playerGameObject.transform : null;
            }

            this.InputModule.GestureDetected += this.OnGestureDetected;
        }

        public void StopSession()
        {
            if (this.loadedSession == null)
            {
                return;
            }

            this.InputModule.GestureDetected -= this.OnGestureDetected;
            this.loadedSession = null;
        }

        private void OnGestureDetected(IGestureData gestureData, List<GameObject> receivers)
        {
            Debug.LogFormat(
                "Gesture detected while playing: {0} (Inside session: {1})",
                Time.time,
                Time.time - this.startTime + this.loadedSession.Keys[0].Time);
        }

        [ContextMenu("Play Session")]
        public void PlaySession()
        {
            if (this.loadedSession == null)
            {
                Debug.LogWarning("No session loaded");
                return;
            }

            if (this.InputModule == null)
            {
                Debug.LogWarning("No input module set");
                return;
            }

            // Add session gesture recognizer to input module.
            this.PlaySession(this.loadedSession);
        }

        public void FixedUpdate()
        {
            foreach (var scheduledGesture in this.scheduledGestures)
            {
                if (scheduledGesture.Executed)
                {
                    continue;
                }

                if (scheduledGesture.Time < Time.time)
                {
                    // Resolve receivers.
                    var receivers = scheduledGesture.Receivers.Select<string, GameObject>(GameObjectUtility.GetGameObject).ToList();

                    // Force player orientation.
                    if (this.playerTransform != null)
                    {
                        this.playerTransform.rotation = scheduledGesture.PlayerOrientation;
                    }

                    this.InputModule.SendGesture(scheduledGesture.GestureData, receivers);
                    scheduledGesture.Executed = true;
                }
            }
        }

        private class ScheduledGesture
        {
            public bool Executed;

            public IGestureData GestureData;

            public Quaternion PlayerOrientation;

            public List<string> Receivers;

            public float Time;
        }
    }
}