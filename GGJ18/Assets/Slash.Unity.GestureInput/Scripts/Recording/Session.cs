﻿namespace Slash.Unity.GestureInput.Recording
{
    using System;
    using System.Collections.Generic;

    using UnityEngine;

    [Serializable]
    public class SessionGesture
    {
        public string Data;

        /// <summary>
        ///   Key this gesture is relative to.
        /// </summary>
        public string Key;

        public Quaternion PlayerOrientation;

        /// <summary>
        ///   Paths of game objects that the gesture should be send to.
        /// </summary>
        public List<string> Receivers;

        /// <summary>
        ///   Relative time after key this gesture was triggered.
        /// </summary>
        public float RelativeTime;

        public string Type;
    }

    [Serializable]
    public class Session
    {
        public List<SessionGesture> Gestures;

        public List<SessionKey> Keys;

        /// <summary>
        ///   Path to player transform path that should be recorded.
        /// </summary>
        public string PlayerTransform;
    }

    [Serializable]
    public class SessionKey
    {
        public string Name;

        public float Time;
    }
}