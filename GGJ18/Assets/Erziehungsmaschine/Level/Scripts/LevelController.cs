﻿using System.Collections.Generic;
using System.Linq;
using Erziehungsmaschine.Constructions.Signals;
using Erziehungsmaschine.Flow;
using Erziehungsmaschine.Humans;
using Erziehungsmaschine.Sounds.Scripts;
using UnityEngine;

namespace Erziehungsmaschine.Level
{
    public class LevelController
    {
        private readonly SoundSettingsScriptableObject.AudioSettings audioSettings;

        private readonly HumansController humanSpawner;

        private readonly LevelModel levelModel;

        private readonly SoundController soundController;

        private readonly TriggerCutsceneSignal triggerCutsceneSignal;

        private readonly UnlockConstructionSignal unlockConstructionSignal;

        public LevelController(LevelModel levelModel, TriggerCutsceneSignal triggerCutsceneSignal,
            UnlockConstructionSignal unlockConstructionSignal, HumansController humanSpawner,
            SoundController soundController,
            SoundSettingsScriptableObject.AudioSettings audioSettings)
        {
            this.levelModel = levelModel;
            this.triggerCutsceneSignal = triggerCutsceneSignal;
            this.unlockConstructionSignal = unlockConstructionSignal;
            this.humanSpawner = humanSpawner;
            this.soundController = soundController;
            this.audioSettings = audioSettings;
        }

        public void AddCorrectHuman()
        {
            // Add count to first non-fulfilled goal.
            foreach (var goal in levelModel.Goals)
            {
                if (!goal.IsFulfilled)
                {
                    goal.CurrentCount++;
                }
            }

            CheckWinFailCondition();
        }

        public void AddWrongHuman()
        {
            levelModel.WrongHumanCount++;
            CheckWinFailCondition();
        }

        public void GameOver(bool win)
        {
            levelModel.IsLevelFinished = true;
            levelModel.OnGameOver(win);
        }

        public void HumanReachedExit(HumanModel human)
        {
            soundController.PlayClip(audioSettings.KaChing);
            if (levelModel.IsLevelFinished)
            {
                StartNextLevel();
                return;
            }

            var humanFulfilledGoal = false;
            foreach (var levelGoal in levelModel.Goals)
            {
                if (levelGoal.NeededAttributes.FulfilledBy(human.Attributes))
                {
                    levelGoal.CurrentCount++;
                    humanFulfilledGoal = true;
                }
            }

            if (!humanFulfilledGoal)
            {
                AddWrongHuman();
            }

            levelModel.FireScoreChanged();
            CheckWinFailCondition();
        }

        public void LoadLevel(LevelConfigScriptableObject config)
        {
            levelModel.Config = config;
            levelModel.IsLevelFinished = false;
            levelModel.Goals = config.Goals.Select(goalConfig =>
                    new LevelGoal
                    {
                        NeededAttributes = goalConfig.NeededAttributes,
                        NeededCount = goalConfig.NeededCount
                    })
                .ToList();
            levelModel.WrongHumanCount = 0;

            // Unlock required constructions.
            var requiredConstructions = new HashSet<string>();
            foreach (var goal in config.Goals)
            {
                if (goal.NeededAttributes.HeadAttribute != HumanHeadAttribute.Any)
                {
                    requiredConstructions.Add("head");
                }

                if (goal.NeededAttributes.FaceAttribute != HumanFaceAttribute.Any)
                {
                    requiredConstructions.Add("face");
                }

                if (goal.NeededAttributes.BodyAttribute != HumanBodyAttribute.Any)
                {
                    requiredConstructions.Add("body");
                }

                if (goal.NeededAttributes.ObjectAttribute != HumanObjectAttribute.Any)
                {
                    requiredConstructions.Add("object");
                }
            }

            // Remove constructions unlocked during cut scene.
            if (config.Cutscene != null)
            {
                foreach (var cutsceneStep in config.Cutscene.Steps)
                {
                    requiredConstructions.Remove(cutsceneStep.UnlockConstructionId);
                }
            }

            foreach (var unlockedConstruction in requiredConstructions)
            {
                unlockConstructionSignal.Fire(unlockedConstruction);
            }

            // Setup human spawn settings.
            humanSpawner.Settings.HumanSpawnTime = config.HumanSpawnTime;

            if (config.Cutscene != null && config.Cutscene.Steps.Count > 0)
            {
                triggerCutsceneSignal.Fire(levelModel.Config.Cutscene, OnCutsceneFinished);
            }
            else
            {
                OnCutsceneFinished();
            }
        }

        public void WinLevel()
        {
            Debug.Log("LEVEL WON");
            levelModel.IsLevelFinished = true;
            if (levelModel.Config.Outro != null && levelModel.Config.Outro.Steps.Count > 0)
            {
                triggerCutsceneSignal.Fire(levelModel.Config.Outro, OnOutroFinished);
            }
            else
            {
                StartNextLevel();
            }
        }

        private void CheckWinFailCondition()
        {
            if (levelModel.HasWon())
            {
                WinLevel();
            }
            else if (levelModel.HasLost())
            {
                levelModel.IsLevelFinished = true;
                Debug.LogError("WE LOST...");
                levelModel.OnGameOver(false);
            }
        }

        private void OnCutsceneFinished()
        {
            CheckWinFailCondition();
        }

        private void OnOutroFinished()
        {
            StartNextLevel();
        }

        private void StartNextLevel()
        {
            Debug.Log("Ready to start next level");

            var nextLevel = levelModel.Config.NextLevel;
            if (nextLevel != null)
            {
                // Start the next level.
                LoadLevel(nextLevel);
            }
            else
            {
                Debug.Log("GAME WON");
                levelModel.OnGameOver(true);
            }
        }
    }
}