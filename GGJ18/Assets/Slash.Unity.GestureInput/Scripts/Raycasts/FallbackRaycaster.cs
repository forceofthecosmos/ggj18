﻿namespace Slash.Unity.GestureInput.Raycasts
{
    using System.Collections.Generic;

    using UnityEngine;
    using UnityEngine.EventSystems;

    /// <summary>
    ///   Raycaster which hits its own game object to use it for all events that fall through.
    /// </summary>
    public class FallbackRaycaster : BaseRaycaster
    {
        /// <summary>
        ///   Sorting layer to use for hit result.
        /// </summary>
        public int SortingLayer;

        /// <summary>
        ///   Sorting order, only relevant if there are multiple fallback raycasters.
        /// </summary>
        public int SortOrder = int.MinValue;

        /// <summary>
        ///   Fallback target to use for input.
        /// </summary>
        [Tooltip("Fallback target to use for input.")]
        public GameObject Target;

        private RaycastResult result;

        public override Camera eventCamera
        {
            get
            {
                return null;
            }
        }

        public override int renderOrderPriority
        {
            get
            {
                return int.MinValue;
            }
        }

        public override int sortOrderPriority
        {
            get
            {
                return this.SortOrder;
            }
        }

        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            this.result.depth = int.MinValue;
            this.result.distance = float.MaxValue;
            this.result.gameObject = this.Target;
            this.result.index = int.MaxValue;
            this.result.module = this;
            this.result.sortingLayer = this.SortingLayer;
            this.result.sortingOrder = this.SortOrder;
            resultAppendList.Add(this.result);
        }
    }
}