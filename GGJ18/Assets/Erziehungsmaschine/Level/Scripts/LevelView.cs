﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Erziehungsmaschine.Level.Scripts
{
    public class LevelView : MonoBehaviour
    {
        [SerializeField] private LevelGoalView[] goalViews;

        private LevelModel levelModel;

        [SerializeField] private Text wrongHumanText;

        [Inject]
        public void Initialize(LevelModel levelModel)
        {
            this.levelModel = levelModel;

            levelModel.ScoreChanged += OnScoreChanged;
            levelModel.ConfigChanged += OnConfigChanged;
            OnConfigChanged();
            OnScoreChanged();
        }

        private void OnConfigChanged()
        {
            SetupGoalHumanView();
        }

        private void OnScoreChanged()
        {
            for (var i = 0; i < goalViews.Length; i++)
            {
                goalViews[i].UpdateScore(levelModel.Goals != null && i < levelModel.Goals.Count ? levelModel.Goals[i] : null);
            }
            
            wrongHumanText.text = "Wrong: " + levelModel.WrongHumanCount;
        }

        private void SetupGoalHumanView()
        {
            var levelConfig = levelModel.Config;
            for (var i = 0; i < goalViews.Length; i++)
            {
                goalViews[i].Setup(levelConfig != null && i < levelConfig.Goals.Count ? levelConfig.Goals[i] : null);
            }
        }
    }
}