﻿using Erziehungsmaschine.Humans;

namespace Erziehungsmaschine.Conversions
{
    public class ChangeBodyAttributeConversion : ChangeAttributeConversion<HumanBodyAttribute>
    {
        public ChangeBodyAttributeConversion() : base((attributes, value) => attributes.BodyAttribute = value,
            attributes => attributes.BodyAttribute)
        {
        }

        protected override HumanBodyAttribute Wildcard
        {
            get { return HumanBodyAttribute.Any; }
        }
    }
}