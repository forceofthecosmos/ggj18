﻿using UnityEngine;

namespace Erziehungsmaschine.Humans
{
    public class HumanController
    {
        public HumanController(HumanModel model)
        {
            this.Model = model;
        }

        public HumanModel Model { get; private set; }

        public void SetPosition(Vector2 worldPosition)
        {
            Model.DoTeleport(worldPosition);
        }
    }
}