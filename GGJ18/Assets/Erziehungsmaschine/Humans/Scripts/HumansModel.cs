﻿using System.Collections.Generic;
using System.Linq;

namespace Erziehungsmaschine.Humans
{
    public class HumansModel
    {
        private readonly List<HumanView> humanViews = new List<HumanView>();

        public bool IsGameRunning;

        public float NextHumanSpawnTimer;

        public IEnumerable<HumanView> Humans
        {
            get { return humanViews; }
        }

        /// <summary>
        ///   Indicates if spawning is currently paused.
        /// </summary>
        public bool IsSpawningPaused;

        public void AddHuman(HumanView human)
        {
            humanViews.Add(human);
        }

        public HumanView GetHumanView(HumanModel human)
        {
            return humanViews.FirstOrDefault(view => view.Model == human);
        }

        public void RemoveHuman(HumanView human)
        {
            humanViews.Remove(human);
        }
    }
}