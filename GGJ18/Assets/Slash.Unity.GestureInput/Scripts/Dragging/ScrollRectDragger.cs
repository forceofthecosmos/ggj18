﻿namespace Slash.Unity.GestureInput.Scripts.Dragging
{
    using Slash.Unity.GestureInput.Gestures.Implementations;
    using Slash.Unity.GestureInput.Sources;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    /// <summary>
    ///     Connects a <see cref="DragSource" /> to a <see cref="ScrollRect" />.
    /// </summary>
    public class ScrollRectDragger : MonoBehaviour
    {
        public DragSource DragSource;

        public ScrollRect ScrollRect;

        public void OnDisable()
        {
            if (this.DragSource != null)
            {
                this.DragSource.BeginDrag.RemoveListener(this.OnBeginDrag);
                this.DragSource.Drag.RemoveListener(this.OnDrag);
                this.DragSource.EndDrag.RemoveListener(this.OnEndDrag);
            }
        }

        public void OnEnable()
        {
            if (this.DragSource != null)
            {
                this.DragSource.BeginDrag.AddListener(this.OnBeginDrag);
                this.DragSource.Drag.AddListener(this.OnDrag);
                this.DragSource.EndDrag.AddListener(this.OnEndDrag);
            }
        }

        private void OnBeginDrag(BeginDragEventData eventData)
        {
            if (this.ScrollRect != null)
            {
                var pointerEventData = new PointerEventData(null)
                {
                    button = PointerEventData.InputButton.Left,
                    position = eventData.Position
                };
                this.ScrollRect.OnBeginDrag(pointerEventData);
            }
        }

        private void OnDrag(DragEventData eventData)
        {
            if (this.ScrollRect != null)
            {
                var pointerEventData = new PointerEventData(null)
                {
                    button = PointerEventData.InputButton.Left,
                    position = eventData.Position
                };
                this.ScrollRect.OnDrag(pointerEventData);
            }
        }

        private void OnEndDrag(EndDragEventData eventData)
        {
            if (this.ScrollRect != null)
            {
                var pointerEventData = new PointerEventData(null) {button = PointerEventData.InputButton.Left};
                this.ScrollRect.OnEndDrag(pointerEventData);
            }
        }

        public void Reset()
        {
            if (this.DragSource == null)
            {
                this.DragSource = this.GetComponent<DragSource>();
            }
            if (this.ScrollRect == null)
            {
                this.ScrollRect = this.GetComponent<ScrollRect>();
            }
        }
    }
}