﻿using System;
using System.Collections;
using Erziehungsmaschine.Zenject.Installers;
using UnityEngine;
using UnityEngine.UI;

namespace Erziehungsmaschine.Menu
{
    public class EntryMenuFade : MonoBehaviour
    {
        [SerializeField]
        private Button startButton;

        [SerializeField]
        private Button creditsButton;

        [SerializeField]
        private GameObject logo;

        [SerializeField]
        private EntryPoint entryPoint;

        [SerializeField]
        private Camera mainCamera;

        [SerializeField]
        private Vector3 targetPosition;

        [SerializeField] private float tweenSpeed = 1;
        
        [SerializeField] private CreditsManager creditsManager;

        private void Start()
        {
            startButton.onClick.AddListener(OnStartButton);
            creditsButton.onClick.AddListener(OnShowCredits);
        }

        private void OnShowCredits()
        {
            creditsManager.gameObject.SetActive(true);
        }

        private void OnStartButton()
        {
            StartCoroutine(StartSequence());
        }

        private IEnumerator StartSequence()
        {
            startButton.gameObject.SetActive(false);
            creditsButton.gameObject.SetActive(false);
            logo.SetActive(false);

            while (true)
            {
                var distance = Vector3.Distance(mainCamera.transform.position, targetPosition);
                float timedSpeed = tweenSpeed * Time.deltaTime;
                if (distance < timedSpeed)
                {
                    // Reached target
                    FinishSequence();
                    yield break;
                }
                
                var targetVector = targetPosition - mainCamera.transform.position;
                mainCamera.transform.position += targetVector.normalized * timedSpeed;
                yield return null;
            }
        }

        public void FinishSequence()
        {
            logo.SetActive(false);
            startButton.gameObject.SetActive(false);
            creditsButton.gameObject.SetActive(false);
            mainCamera.transform.position = targetPosition;
            entryPoint.StartGame();
        }
    }
}