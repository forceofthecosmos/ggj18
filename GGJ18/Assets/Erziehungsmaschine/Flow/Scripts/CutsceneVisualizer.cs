﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class CutsceneVisualizer : MonoBehaviour
    {
        public Image Picture;

        public List<Sprite> PoseImages;

        private Animator animator;

        private TextMeshProUGUI text;

        public void FadeIn()
        {
            ShowText(string.Empty);
            animator.SetBool("Is Visible", true);
        }

        public void FadeOut()
        {
            animator.SetBool("Is Visible", false);
        }

        [Inject]
        public void Initializer(Animator animator, TextMeshProUGUI text)
        {
            this.animator = animator;
            this.text = text;
        }

        public void ShowText(string text)
        {
            this.text.text = text;
            if (PoseImages.Count > 0)
            {
                Picture.sprite = PoseImages[Random.Range(0, PoseImages.Count)];
            }
        }
    }
}