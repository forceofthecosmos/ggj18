﻿using System.Linq;
using Erziehungsmaschine.Humans;
using Erziehungsmaschine.Sounds.Scripts;
using Slash.Unity.GestureInput.Dragging;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Constructions
{
    public class ConstructionView : MonoBehaviour
    {
        /// <summary>
        ///     Duration to convert a human in this construction (in s).
        /// </summary>
        [Tooltip("Duration to convert a human in this construction (in s)")] [Range(0.1f, 30.0f)]
        public float ConversionDuration = 5.0f;
        
        public DropTarget DropTarget;

        /// <summary>
        ///     Position where a human exits the construction.
        /// </summary>
        [Tooltip("Position where a human exits the construction")]
        public Transform ExitPosition;

        private ConstructionController controller;

        private ConstructionModel model;

        private ConstructionSettings settings;

        private SoundSettingsScriptableObject.AudioSettings audioSettings;

        private SoundController soundController;

        [Inject]
        public void Construct(ConstructionController controllerInject, ConstructionModel modelInject,
            ConstructionSettings settingsInject, SoundController soundController, SoundSettingsScriptableObject.AudioSettings audioSettings)
        {
            this.soundController = soundController;
            this.audioSettings = audioSettings;
            controller = controllerInject;
            model = modelInject;

            if (DropTarget == null)
            {
                DropTarget = gameObject.AddComponent<DropTarget>();
            }

            if (ExitPosition == null)
            {
                ExitPosition = transform;
            }

            // Setup settings.
            settings = settingsInject;
            settings.ConversionDuration = ConversionDuration;
            settings.ExitPosition = ExitPosition.transform.position;
        }

        public void OnDrop(DropEventData data)
        {
            soundController.PlayClip(audioSettings.DropOnConstruct);
            // Check if human was dropped on construction.
            var draggedHumanView = data.DragObjects.Select(dragObject => dragObject.GetComponent<HumanView>())
                .FirstOrDefault(humanView => humanView != null);
            if (draggedHumanView == null)
            {
                Debug.LogWarningFormat(this, "Dropped a non-human on the construction");
                return;
            }

            controller.DropHuman(draggedHumanView.Controller);
        }

        protected void OnDisable()
        {
            DropTarget.Drop.RemoveListener(OnDrop);
        }

        protected void OnEnable()
        {
            DropTarget.Drop.AddListener(OnDrop);
        }
    }
}