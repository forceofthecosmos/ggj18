﻿namespace Slash.Unity.GestureInput.Modules
{
    using System;
    using System.Collections.Generic;

    using Slash.Unity.GestureInput.Gestures;

    using UnityEngine;
    using UnityEngine.EventSystems;

    public class RegisteredInputSource<TSource, TEventData> : RegisteredInputSource
        where TSource : class, IEventSystemHandler where TEventData : IGestureData
    {
        /// <summary>
        ///   Shared list to avoid allocation.
        /// </summary>
        private static readonly List<TSource> Sources = new List<TSource>();

        private readonly Func<TSource, TEventData, bool> eventFunctor;

        public RegisteredInputSource(Func<TSource, TEventData, bool> eventFunctor)
        {
            this.eventFunctor = eventFunctor;
        }

        public override Type GetEventDataType()
        {
            return typeof(TEventData);
        }

        public override bool Execute(IGestureData eventData, IEnumerable<GameObject> currentPointedGameObjects)
        {
            foreach (var currentPointedGameObject in currentPointedGameObjects)
            {
                if (this.Execute((TEventData)eventData, currentPointedGameObject))
                {
                    return true;
                }
            }
            return false;
        }

        public override bool TryHandle(IGestureData eventData, GameObject target)
        {
            Sources.Clear();
            GetSources(target, Sources);

            var handled = false;
            for (var i = 0; i < Sources.Count; i++)
            {
                var source = Sources[i];
                try
                {
                    if (this.eventFunctor(source, (TEventData)eventData))
                    {
                        handled = true;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            return handled;
        }

        private bool Execute(TEventData eventData, GameObject root)
        {
            if (root == null)
            {
                return false;
            }

            var target = root.transform;
            var handled = false;
            while (target != null)
            {
                handled = TryHandle(eventData, target.gameObject);
                if (handled)
                {
                    break;
                }

                target = target.parent;
            }

            return handled;
        }

        private static void GetSources(GameObject go, ICollection<TSource> results)
        {
            if (results == null)
            {
                throw new ArgumentException("Results array is null", "results");
            }

            if (go == null || !go.activeInHierarchy)
            {
                return;
            }

            Components.Clear();
            go.GetComponents(Components);
            for (var i = 0; i < Components.Count; i++)
            {
                var component = Components[i];

                var source = component as TSource;
                if (source == null)
                {
                    continue;
                }

                var behaviour = component as Behaviour;
                if (behaviour != null && !behaviour.isActiveAndEnabled)
                {
                    continue;
                }

                results.Add(source);
            }
        }
    }

    public abstract class RegisteredInputSource
    {
        /// <summary>
        ///   Shared list to avoid allocation.
        /// </summary>
        protected static readonly List<Component> Components = new List<Component>();

        /// <summary>
        ///   Handles the specified event on the specified pointed game objects.
        /// </summary>
        /// <param name="eventData">Event to handle.</param>
        /// <param name="currentPointedGameObjects">Game objects to give the chance to handle the event.</param>
        /// <returns>True if event was handled; otherwise, false.</returns>
        public abstract bool Execute(IGestureData eventData, IEnumerable<GameObject> currentPointedGameObjects);

        public abstract Type GetEventDataType();

        /// <summary>
        ///   Tries to handle the specified event on the specified game object.
        /// </summary>
        /// <param name="eventData">Event to handle.</param>
        /// <param name="target">Game object to give the chance to handle the event.</param>
        /// <returns>True if game object handled event; otherwise, false.</returns>
        public abstract bool TryHandle(IGestureData eventData, GameObject target);
    }
}