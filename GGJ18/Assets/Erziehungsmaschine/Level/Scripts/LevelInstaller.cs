﻿using Zenject;

namespace Erziehungsmaschine.Level.Scripts
{
    public class LevelInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<LevelController>().AsSingle();
            Container.Bind<LevelModel>().AsSingle();
        }
    }
}