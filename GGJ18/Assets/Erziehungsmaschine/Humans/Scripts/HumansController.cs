using System.Collections;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Erziehungsmaschine.Humans
{
    public class HumansController : ITickable
    {
        private readonly DiContainer diContainer;

        private readonly HumansModel humansModel;

        private HumansSettings settings;

        private IEnumerator spawnCoroutine;

        private int spawnedHumanCount;

        public HumansController(
            HumansModel humansModel,
            DiContainer diContainer)
        {
            this.humansModel = humansModel;
            this.diContainer = diContainer;
        }

        public HumansSettings Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = Object.FindObjectOfType<HumansSettings>();
                }

                return settings;
            }
        }

        /// <summary>
        ///     Despawns the human from the game world.
        /// </summary>
        /// <param name="human">Human to despawn.</param>
        public void Despawn(HumanModel human)
        {
            var view = humansModel.GetHumanView(human);
            if (view != null)
            {
                Despawn(view);
            }
            else
            {
                Debug.LogWarningFormat("Can't despawn '{0}', no view found", human);
            }
        }

        public void HumanEnterTargetExitArea(HumanView view)
        {
            if (view.Model.IsDragged == false)
            {
                return;
            }
            SetNextTargetForHuman(view, true, false);
        }

        public void HumanExitTargetExitArea(HumanView view)
        {
            if (view.Model.IsDragged == false)
            {
                return;
            }

            SetNextTargetForHuman(view, false, false);
        }

        /// <summary>
        ///     Spawns the human at the specified world position.
        /// </summary>
        /// <param name="human">Human to spawn.</param>
        /// <param name="worldPosition">Position to spawn the human at.</param>
        public void Spawn(HumanModel human, Vector2 worldPosition)
        {
            var view = humansModel.GetHumanView(human);
            if (view == null)
            {
                SpawnHuman(worldPosition, human);
            }
            else
            {
                Debug.LogWarningFormat("Can't spawn '{0}', already spawned", human);
            }
        }

        public HumanView SpawnHuman()
        {
            return SpawnHuman(Settings.SpawnPosition.transform.position);
        }

        public void StartGame()
        {
            humansModel.IsGameRunning = true;
            humansModel.NextHumanSpawnTimer = 0; //Settings.HumanSpawnTime;
        }


        public void Tick()
        {
            if (humansModel.IsGameRunning == false)
            {
                return;
            }

            CheckHumanSpawning();

            CheckHumanWaitingTime();
        }

        private void CheckHumanSpawning()
        {
            if (humansModel.IsSpawningPaused)
            {
                return;
            }

            if (humansModel.NextHumanSpawnTimer > 0)
            {
                humansModel.NextHumanSpawnTimer -= Time.deltaTime;
                return;
            }

            SpawnHuman();

            humansModel.NextHumanSpawnTimer = Settings.HumanSpawnTime;
        }

        private void CheckHumanWaitingTime()
        {
            foreach (var humanView in humansModel.Humans)
            {
                var model = humanView.Model;

                if (model.IsWalking || model.IsSaluting || model.IsDragged)
                {
                    continue;
                }

                if (model.WaitingTime > 0)
                {
                    model.WaitingTime -= Time.deltaTime;
                    continue;
                }

                model.IsWalking = true;
                SetNextTargetForHuman(humanView);
            }
        }

        private void Despawn(HumanView humanView)
        {
            humanView.AgentReachedDestination -= OnHumanViewOnAgentReachedDestination;
            humanView.Dispose();
            humansModel.RemoveHuman(humanView);
            Object.Destroy(humanView.gameObject);
        }

        private HumanView SpawnHuman(Vector3 spawnPosition, HumanModel model = null)
        {
            string humanName;
            if (model == null)
            {
                humanName = string.Format("Human {0}", spawnedHumanCount + 1);

                model = new HumanModel {DebugName = humanName, IsWalking = true};

                // Only do this if its a new human
                model.Attributes.Randomize();
                model.DistractionsBeforeExit = Random.Range(3, 6);

                ++spawnedHumanCount;
            }
            else
            {
                humanName = model.DebugName;
            }

            // Instantiate new human object.
            diContainer.BindInstance(model);
            var humanObj = diContainer.InstantiatePrefab(Settings.HumanPrefab,
                spawnPosition, Quaternion.identity, Settings.HumansContainer.transform);
            humanObj.name = humanName;
            diContainer.Unbind<HumanModel>();

            
            var humanView = humanObj.GetComponent<HumanView>();
            SetNextTargetForHuman(humanView);
            
            humanView.AgentReachedDestination += OnHumanViewOnAgentReachedDestination;
            humansModel.AddHuman(humanView);

            return humanView;
        }

        private void OnHumanViewOnAgentReachedDestination(HumanView humanView)
        {
            humanView.Model.IsWalking = false;
            humanView.Model.WaitingTime = Random.Range(3, 7);
        }

        private void SetNextTargetForHuman(HumanView human, bool forceToExit = false, bool isReducingDistractionCount = true)
        {
            Vector3 targetPosition;
            if (forceToExit || human.Model.DistractionsBeforeExit <= 0)
            {
                targetPosition = Settings.HumanEndPosition.transform.position;
            }
            else
            {
                // Choose a random new waypoint
                var wayPointCount = Settings.HumansWalkPoints.Length;
                var randomWaypoint = Settings.HumansWalkPoints[Random.Range(0, wayPointCount)];
                targetPosition = randomWaypoint.transform.position;
                if (isReducingDistractionCount)
                {
                    human.Model.DistractionsBeforeExit--;
                }
            }
            human.Model.TargetPosition = targetPosition;
        }

        public void StartSaluting()
        {
            humansModel.IsSpawningPaused = true;
            foreach (var human in humansModel.Humans)
            {
                human.Model.IsWalking = false;
                human.Model.IsSaluting = true;
            }
        }

        public void StopSaluting()
        {
            foreach (var human in humansModel.Humans)
            {
                human.Model.IsSaluting = false;
                human.Model.IsWalking = true;
            }
            humansModel.IsSpawningPaused = false;
        }
    }
}