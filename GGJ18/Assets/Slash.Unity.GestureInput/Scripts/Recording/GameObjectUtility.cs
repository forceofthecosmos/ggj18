﻿using UnityEngine;

namespace Slash.Unity.GestureInput.Recording
{
    public class GameObjectUtility
    {
        public static GameObject GetGameObject(string path)
        {
            return GameObject.Find(path);
        }

        public static string GetPath(GameObject gameObject)
        {
            var path = "/" + gameObject.name;
            var transform = gameObject.transform;

            while (transform.parent != null)
            {
                transform = transform.parent;
                path = "/" + transform.gameObject.name + path;
            }
            return path;
        }
    }
}