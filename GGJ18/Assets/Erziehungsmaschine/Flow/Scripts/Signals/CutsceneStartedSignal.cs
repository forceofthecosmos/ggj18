﻿using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class CutsceneStartedSignal : Signal<CutsceneStartedSignal>
    {
    }
}