﻿using TMPro;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class SupervisorInstaller : MonoInstaller
    {
        public Animator CutsceneAnimator;

        public TextMeshProUGUI CutsceneText;

        public CutsceneVisualizer CutsceneVisualizer;

        public override void InstallBindings()
        {
            Container.BindInstance(CutsceneAnimator);
            Container.BindInstance(CutsceneText);

            Container.BindInstance(CutsceneVisualizer).AsSingle();
            Container.Bind<CutsceneModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<CutsceneController>().AsSingle();
        }
    }
}