﻿using System.Collections;
using System.Collections.Generic;
using Erziehungsmaschine.Humans;
using UnityEngine;
using Zenject;

public class TargetExitView : MonoBehaviour
{
    private HumansController humansController;

    [Inject]
    public void Initialize(HumansController humansController)
    {
        this.humansController = humansController;
    }
        
    private void OnTriggerEnter2D(Collider2D other)
    {
        var humanView = other.transform.GetComponent<HumanView>();
        if (humanView == null)
        {
            return;
        }
        humansController.HumanEnterTargetExitArea(humanView);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var humanView = other.transform.GetComponent<HumanView>();
        if (humanView == null)
        {
            return;
        }
        humansController.HumanExitTargetExitArea(humanView);
    }
}
