﻿using Zenject;

namespace Erziehungsmaschine.Constructions.Signals
{
    public class UnlockConstructionSignal : Signal<UnlockConstructionSignal, string>
    {
    }
}