﻿using System.Linq;
using Slash.Unity.GestureInput.Dragging;

namespace Slash.Unity.GestureInput.Gestures.Implementations
{
    using System;
    using System.Collections.Generic;
    using Slash.Unity.GestureInput.Devices;
    using Slash.Unity.GestureInput.Modules;
    using Slash.Unity.GestureInput.Raycasts;
    using Slash.Unity.GestureInput.Sources;
    using Slash.Unity.GestureInput.Utils;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class DragEventData : BaseEventData, IGestureData
    {
        public DragEventData()
            : base(null)
        {
        }

        /// <summary>
        ///     Movement (in pixels).
        /// </summary>
        public Vector2 Delta { get; set; }

        /// <summary>
        ///     Movement (in mm).
        /// </summary>
        public Vector2 DeltaPhysical { get; set; }

        /// <summary>
        ///     Current pointer position (in screen coordinates).
        /// </summary>
        public Vector2 Position { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Format("DeltaPhysical: {0}, Delta: {1}, Position: {2}", this.DeltaPhysical, this.Delta,
                this.Position);
        }
    }

    public class BeginDragEventData : BaseEventData, IGestureData
    {
        public BeginDragEventData()
            : base(null)
        {
        }

        /// <summary>
        ///     Current pointer position (in screen coordinates).
        /// </summary>
        public Vector2 Position { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Format("Position: {0}", this.Position);
        }
    }

    public class EndDragEventData : BaseEventData, IGestureData
    {
        public EndDragEventData()
            : base(null)
        {
        }

        /// <summary>
        ///     Velocity the drag was performed with.
        /// </summary>
        public Vector2 DragVelocity { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Format("DragVelocity: {0}", this.DragVelocity);
        }
    }

    [Serializable]
    public class DragGestureRecognizer : GestureRecognizer<DragEventData>
    {
        private readonly List<GameObject> dragObjects = new List<GameObject>();

        /// <summary>
        ///     Pointer move threshold before drag begins (in mm).
        /// </summary>
        [Tooltip("Pointer move threshold before drag begins (in mm)")]
        public float DragThreshold = 4;

        private Vector2 dragVelocity;

        /// <summary>
        ///     Indicates if drag was canceled (e.g. by second touch).
        /// </summary>
        private bool isCanceled;

        private bool isDragging;

        /// <summary>
        ///     Indicates if verbose logging is enabled.
        /// </summary>
        public bool IsLoggingEnabled;

        private Vector2 lastDragPosition;

        private float lastDragTimestamp;

        private IPointerDevice pointerDevice;

        private IPointerTouchDetection pointerTouchDetection;

        private Vector2 pressPosition;

        /// <summary>
        ///     Indicates if drag events are send to Unity UI event handlers:
        ///     IBeginDragHandler, IDragHandler, IEndDragHandler
        /// </summary>
        [Tooltip(
            "Indicates if drag events are send to Unity UI event handlers: IBeginDragHandler, IDragHandler, IEndDragHandler")]
        public bool SendToUnityUIHandlers;

        private bool wasPressed;
        private readonly List<GameObject> dropObjects = new List<GameObject>();

        public override IEnumerable<RegisteredInputSource> GetInputSources()
        {
            yield return new RegisteredInputSource<DragSource, BeginDragEventData>(
                (source, data) => source.OnBeginDrag(data));
            yield return new RegisteredInputSource<DragSource, DragEventData>((source, data) => source.OnDrag(data));
            yield return
                new RegisteredInputSource<DragSource, EndDragEventData>((source, data) => source.OnEndDrag(data));
            yield return
                new RegisteredInputSource<DropTarget, DropEventData>((source, data) => source.OnDrop(data));
            if (this.SendToUnityUIHandlers)
            {
                yield return
                    new RegisteredInputSource<IBeginDragHandler, BeginDragEventData>((source, data) =>
                    {
                        var pointerEventData = new PointerEventData(null)
                        {
                            button = PointerEventData.InputButton.Left,
                            position = data.Position
                        };
                        source.OnBeginDrag(pointerEventData);
                        return true;
                    });
                yield return
                    new RegisteredInputSource<IDragHandler, DragEventData>((source, data) =>
                    {
                        var pointerEventData = new PointerEventData(null)
                        {
                            button = PointerEventData.InputButton.Left,
                            dragging = true,
                            position = data.Position,
                            delta = data.Delta
                        };
                        source.OnDrag(pointerEventData);
                        return true;
                    });
                yield return
                    new RegisteredInputSource<IEndDragHandler, EndDragEventData>((source, data) =>
                    {
                        var pointerEventData = new PointerEventData(null) {button = PointerEventData.InputButton.Left};
                        source.OnEndDrag(pointerEventData);
                        return true;
                    });
            }
        }

        public void Init(IPointerDevice pointerDevice, IPointerTouchDetection pointerTouchDetection)
        {
            this.pointerDevice = pointerDevice;
            this.pointerTouchDetection = pointerTouchDetection;
        }

        public override void Process()
        {
            if (!this.isDragging)
            {
                // Check if drag should begin.
                if (!this.pointerDevice.IsDown())
                {
                    this.wasPressed = false;
                    this.isCanceled = false;
                    return;
                }

                // Cancel drag when more than one touch detected.
                if (Input.touches.Length > 1)
                {
                    this.isCanceled = true;
                    return;
                }

                var position = this.pointerDevice.GetPosition();
                if (!this.wasPressed)
                {
                    this.pressPosition = position;
                    this.wasPressed = true;
                }

                if (!this.isCanceled &&
                    ShouldStartDrag(this.pressPosition, position, this.DragThreshold))
                {
                    this.OnBeginDrag(this.pressPosition);
                }
            }
            else
            {
                // Cancel drag when more than one touch detected.
                if (Input.touches.Length > 1)
                {
                    this.OnEndDrag();
                    return;
                }

                // Update velocity.
                var timestamp = Time.realtimeSinceStartup;
                var elapsedDuration = timestamp - this.lastDragTimestamp;
                this.lastDragTimestamp = timestamp;
                var position = this.pointerDevice.GetPosition();
                var delta = position - this.lastDragPosition;
                this.lastDragPosition = position;

                if (elapsedDuration > 0)
                {
                    var velocity = delta / elapsedDuration;
                    this.dragVelocity = velocity * 0.8f + this.dragVelocity * 0.2f;
                }

                if (delta.sqrMagnitude > 0)
                {
                    // Drag gesture continued.
                    this.OnGestureDetected(
                        new DragEventData
                        {
                            Position = position,
                            Delta = delta,
                            DeltaPhysical = delta / MeasurementUtils.PixelsPerMillimeter
                        },
                        this.dragObjects);
                }

                // Check if drag ended.
                if (!this.pointerDevice.IsDown())
                {
                    this.OnEndDrag();
                }
            }
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Format("DragObjects: {0}", this.dragObjects.Implode(";"));
        }

        private void OnBeginDrag(Vector2 dragStartPosition)
        {
            this.isDragging = true;

            this.dragVelocity = Vector2.zero;
            this.lastDragPosition = dragStartPosition;
            this.pointerTouchDetection.GetTouchedGameObjects(this.lastDragPosition, this.dragObjects);

            if (this.IsLoggingEnabled)
            {
                Debug.LogFormat("Drag begins at position {0} on objects {1}", this.lastDragPosition,
                    this.dragObjects.Implode(";"));
            }

            this.OnGestureDetected(new BeginDragEventData {Position = this.lastDragPosition}, this.dragObjects);
        }

        private void OnEndDrag()
        {
            if (this.IsLoggingEnabled)
            {
                Debug.Log("Drag ended with velocity " + this.dragVelocity);
            }

            this.isDragging = false;

            this.OnGestureDetected(new EndDragEventData {DragVelocity = this.dragVelocity}, this.dragObjects);

            // Inform drop targets.
            this.pointerTouchDetection.GetTouchedGameObjects(this.lastDragPosition, this.dropObjects);
            var draggedObjects = dragObjects.Select(dragObject => dragObject.GetComponentInParent<DragSource>())
                .Where(source => source != null).Select(source => source.gameObject).ToList();
            this.OnGestureDetected(new DropEventData() {DragObjects = draggedObjects}, this.dropObjects);
        }

        private static bool ShouldStartDrag(Vector2 pressPos, Vector2 currentPos, float threshold)
        {
            var deltaMove = (pressPos - currentPos).magnitude;

            // Convert to mm.
            deltaMove /= MeasurementUtils.PixelsPerMillimeter;

            return deltaMove >= threshold;
        }
    }
}