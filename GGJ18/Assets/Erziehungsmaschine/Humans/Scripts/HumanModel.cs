using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Erziehungsmaschine.Humans
{
    public class HumanModel
    {
        private HumanAttributeCollection attributes = new HumanAttributeCollection();

        public string DebugName;

        private bool isDragged;

        private bool isSaluting;

        private bool isWalking;

        private Vector2 targetPosition;

        /// <summary>
        ///     Int counter that lets the human look at random attractions before walking to the exit
        /// </summary>
        public int DistractionsBeforeExit { get; set; }

        /// <summary>
        ///     Position to move to (in world units).
        /// </summary>
        public Vector2 TargetPosition
        {
            get { return targetPosition; }
            set
            {
                targetPosition = value;
                FireTargetPositionChanged();
            }
        }

        public HumanAttributeCollection Attributes
        {
            get { return attributes; }
            set { attributes = value; }
        }

        public bool IsSaluting
        {
            get { return isSaluting; }
            set
            {
                if (value == isSaluting)
                {
                    return;
                }

                isSaluting = value;

                OnSalutingStateChanged();
            }
        }

        public bool IsCarrying
        {
            get { return attributes.ObjectAttribute != HumanObjectAttribute.Default; }
        }

        /// <summary>
        /// Time the human is still waiting at one spot before walking to the next waypoint
        /// </summary>
        public float WaitingTime;

        public bool IsWalking
        {
            get { return isWalking; }
            set
            {
                if (value == isWalking)
                {
                    return;
                }

                isWalking = value;

                OnWalkingStateChanged();
            }
        }

        public bool IsDragged
        {
            get { return isDragged; }
            set
            {
                if (value == isDragged)
                {
                    return;
                }

                isDragged = value;

                OnDraggedStateChanged();
            }
        }

        public void CopyFrom(HumanModel human)
        {
            DebugName = human.DebugName;
            TargetPosition = human.TargetPosition;
        }

        public void DoTeleport(Vector2 worldPosition)
        {
            OnTeleport(worldPosition);
        }

        public event Action DraggedStateChanged;

        public event Action SalutingStateChanged;


        public void SetAttributes(HumanHeadAttribute headAttribute, HumanFaceAttribute faceAttribute,
            HumanBodyAttribute bodyAttribute, HumanObjectAttribute objectAttribute,
            HumanMindAttribute mindAttribute)
        {
            Attributes.HeadAttribute = headAttribute;
            Attributes.FaceAttribute = faceAttribute;
            Attributes.BodyAttribute = bodyAttribute;
            Attributes.ObjectAttribute = objectAttribute;
            Attributes.MindAttribute = mindAttribute;
        }

        public event Action TargetPositionChanged;

        public event Action<Vector2> Teleport;

        public override string ToString()
        {
            return string.Format("{0}", DebugName);
        }

        public event Action WalkingStateChanged;

        protected virtual void FireTargetPositionChanged()
        {
            var handler = TargetPositionChanged;
            if (handler != null)
            {
                handler();
            }
        }

        protected virtual void OnDraggedStateChanged()
        {
            var handler = DraggedStateChanged;
            if (handler != null)
            {
                handler();
            }
        }

        protected virtual void OnSalutingStateChanged()
        {
            var handler = SalutingStateChanged;
            if (handler != null)
            {
                handler();
            }
        }

        protected virtual void OnTeleport(Vector2 worldPosition)
        {
            var handler = Teleport;
            if (handler != null)
            {
                handler(worldPosition);
            }
        }

        protected virtual void OnWalkingStateChanged()
        {
            var handler = WalkingStateChanged;
            if (handler != null)
            {
                handler();
            }
        }

        internal void SetRandomAttributes()
        {
            SetAttributes(
                (HumanHeadAttribute) GetRandomEnumEntry<HumanHeadAttribute>(),
                (HumanFaceAttribute) GetRandomEnumEntry<HumanFaceAttribute>(),
                (HumanBodyAttribute) GetRandomEnumEntry<HumanBodyAttribute>(),
                (HumanObjectAttribute) GetRandomEnumEntry<HumanObjectAttribute>(),
                (HumanMindAttribute) GetRandomEnumEntry<HumanMindAttribute>());
        }

        private int GetRandomEnumEntry<T>()
        {
            var enumCount = Enum.GetValues(typeof(T)).Length;
            return Random.Range(0, enumCount);
        }
    }
}