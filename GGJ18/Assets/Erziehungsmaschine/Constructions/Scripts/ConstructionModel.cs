﻿using System;
using Erziehungsmaschine.Conversions;

namespace Erziehungsmaschine.Constructions
{
    public class ConstructionModel
    {
        /// <summary>
        ///     Id of construction.
        /// </summary>
        public string ConstructionId;

        private ConversionProcess conversionProcess;

        private bool isLocked;

        /// <summary>
        ///     Current conversion process which is running in this construction.
        /// </summary>
        public ConversionProcess ConversionProcess
        {
            get { return conversionProcess; }
            set
            {
                if (value == conversionProcess)
                {
                    return;
                }

                conversionProcess = value;

                OnProcessChanged();
            }
        }

        public bool IsLocked
        {
            get { return isLocked; }
            set
            {
                if (value == isLocked)
                {
                    return;
                }

                isLocked = value;

                OnLockedStateChanged();
            }
        }

        /// <summary>
        ///     Indicates that a human entering this construction is removed.
        /// </summary>
        public bool RemovesHuman { get; set; }

        public event Action LockedStateChanged;

        public event Action ProcessChanged;

        public override string ToString()
        {
            return string.Format("ConstructionId: {0}, ConversionProcess: {1}", ConstructionId, ConversionProcess);
        }

        protected virtual void OnLockedStateChanged()
        {
            var handler = LockedStateChanged;
            if (handler != null)
            {
                handler();
            }
        }

        protected virtual void OnProcessChanged()
        {
            var handler = ProcessChanged;
            if (handler != null)
            {
                handler();
            }
        }
    }
}