﻿using Erziehungsmaschine.Level;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class GameOverView : MonoBehaviour
    {
        public GameObject LooseVisual;

        public GameObject Root;

        public GameObject WinVisual;

        private LevelModel levelModel;

        [Inject]
        public void Initialize(LevelModel levelModelInject)
        {
            levelModel = levelModelInject;

            levelModel.GameOver += OnGameOver;
            if (this.Root != null)
            {
                this.Root.SetActive(false);
            }
        }

        private void OnGameOver(bool wonGame)
        {
            if (Root != null)
            {
                Root.SetActive(true);
            }

            if (WinVisual != null)
            {
                WinVisual.SetActive(wonGame);
            }

            if (LooseVisual != null)
            {
                LooseVisual.SetActive(!wonGame);
            }
        }
    }
}