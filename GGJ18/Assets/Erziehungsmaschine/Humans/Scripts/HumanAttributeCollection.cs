using System;
using Random = UnityEngine.Random;

namespace Erziehungsmaschine.Humans
{
    [Serializable]
    public class HumanAttributeCollection
    {
        public HumanBodyAttribute BodyAttribute;

        public HumanFaceAttribute FaceAttribute;

        public HumanHeadAttribute HeadAttribute;

        public HumanMindAttribute MindAttribute;

        public HumanObjectAttribute ObjectAttribute;

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((HumanAttributeCollection) obj);
        }

        /// <summary>
        ///     Checks if the the attributes are fulfilled by the specified attributes.
        /// </summary>
        /// <param name="other">Attributes which should be checked.</param>
        /// <returns>True if this attributes are fulfilled by the specified ones; otherwise, false.</returns>
        public bool FulfilledBy(HumanAttributeCollection other)
        {
            return (HeadAttribute == HumanHeadAttribute.Any || HeadAttribute == other.HeadAttribute) &&
                   (FaceAttribute == HumanFaceAttribute.Any || FaceAttribute == other.FaceAttribute) &&
                   (BodyAttribute == HumanBodyAttribute.Any || BodyAttribute == other.BodyAttribute) &&
                   (ObjectAttribute == HumanObjectAttribute.Any || ObjectAttribute == other.ObjectAttribute) &&
                   (MindAttribute == HumanMindAttribute.Any || MindAttribute == other.MindAttribute);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) HeadAttribute;
                hashCode = (hashCode * 397) ^ (int) FaceAttribute;
                hashCode = (hashCode * 397) ^ (int) BodyAttribute;
                hashCode = (hashCode * 397) ^ (int) ObjectAttribute;
                hashCode = (hashCode * 397) ^ (int) MindAttribute;
                return hashCode;
            }
        }

        public void Randomize()
        {
            SetAttributes(
                (HumanHeadAttribute) GetRandomEnumEntry<HumanHeadAttribute>(),
                (HumanFaceAttribute) GetRandomEnumEntry<HumanFaceAttribute>(),
                (HumanBodyAttribute) GetRandomEnumEntry<HumanBodyAttribute>(),
                (HumanObjectAttribute) GetRandomEnumEntry<HumanObjectAttribute>(),
                (HumanMindAttribute) GetRandomEnumEntry<HumanMindAttribute>());
        }


        public void SetAttributes(HumanHeadAttribute headAttribute, HumanFaceAttribute faceAttribute,
            HumanBodyAttribute bodyAttribute, HumanObjectAttribute carryingAttribute,
            HumanMindAttribute mindAttribute)
        {
            HeadAttribute = headAttribute;
            FaceAttribute = faceAttribute;
            BodyAttribute = bodyAttribute;
            ObjectAttribute = carryingAttribute;
            MindAttribute = mindAttribute;
        }

        protected bool Equals(HumanAttributeCollection other)
        {
            return HeadAttribute == other.HeadAttribute && FaceAttribute == other.FaceAttribute &&
                   BodyAttribute == other.BodyAttribute && ObjectAttribute == other.ObjectAttribute &&
                   MindAttribute == other.MindAttribute;
        }

        private static int GetRandomEnumEntry<T>()
        {
            var enumCount = Enum.GetValues(typeof(T)).Length;

            // Ignore 0 value, as it is a wildcard.
            return Random.Range(1, enumCount);
        }
    }
}