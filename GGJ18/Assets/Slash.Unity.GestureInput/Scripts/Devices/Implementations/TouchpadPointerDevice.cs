﻿namespace Slash.Unity.GestureInput.Devices.Implementations
{
    using System;

    using UnityEngine;

    [Serializable]
    public class TouchPadPointerDevice : IPointerDevice
    {
        /// <summary>
        ///   Indicates if the touchpad is currently pressed.
        /// </summary>
        private bool isDown;

        /// <summary>
        ///   Current screen position.
        /// </summary>
        private Vector2 position;

        /// <inheritdoc />
        public Vector2 GetPosition()
        {
            return this.position;
        }

        /// <inheritdoc />
        public bool IsDown()
        {
            return this.isDown;
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        public void Update()
        {
            if (Input.GetMouseButton(0))
            {
                var newTouchPadPointerPosition = (Vector2)Input.mousePosition;

                if (Input.GetMouseButtonDown(0))
                {
                    this.isDown = true;
                }

                this.position = newTouchPadPointerPosition;
            }
            else
            {
                this.isDown = false;
            }
        }
    }
}