﻿using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class CutsceneDebugger : MonoBehaviour
    {
        public Cutscene Cutscene;

        private TriggerCutsceneSignal triggerCutsceneSignal;

        [Inject]
        public void Initialize(TriggerCutsceneSignal triggerCutsceneSignalInject)
        {
            this.triggerCutsceneSignal = triggerCutsceneSignalInject;
        }

        [ContextMenu("Trigger Cutscene")]
        public void TriggerCutscene()
        {
            triggerCutsceneSignal.Fire(Cutscene, null);
        }
    }
}