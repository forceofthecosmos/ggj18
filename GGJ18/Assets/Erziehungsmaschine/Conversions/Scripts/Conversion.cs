﻿using Erziehungsmaschine.Humans;
using UnityEngine;

namespace Erziehungsmaschine.Conversions
{
    public abstract class Conversion : MonoBehaviour
    {
        public abstract void Execute(HumanAttributeCollection humanAttributes);
    }
}