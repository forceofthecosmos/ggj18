﻿using Erziehungsmaschine.Level;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Humans
{
    public class EndAreaView : MonoBehaviour
    {
        private HumansController humansController;

        private LevelController levelController;

        [Inject]
        public void Initialize(HumansController humansController, LevelController levelController)
        {
            this.humansController = humansController;
            this.levelController = levelController;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var humanView = other.transform.GetComponent<HumanView>();
            if (humanView == null)
            {
                return;
            }

            levelController.HumanReachedExit(humanView.Model);
            humansController.Despawn(humanView.Model);
        }
    }
}