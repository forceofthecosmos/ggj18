﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Misc
{

    public class CoroutineHelper : IInitializable
    {
        private class CoroutineProvider : MonoBehaviour
        {
            
        }

        private CoroutineProvider routineObject;
        
        public void Initialize()
        {
            var obj = new GameObject("CoroutineProvider");
            Object.DontDestroyOnLoad(obj);
            routineObject = obj.AddComponent<CoroutineProvider>();
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            return routineObject.StartCoroutine(routine);
        }

        public void StopCoroutine(Coroutine routine)
        {
            routineObject.StopCoroutine(routine);
        }
    }
}