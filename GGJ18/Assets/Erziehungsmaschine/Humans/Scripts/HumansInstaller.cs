﻿using Erziehungsmaschine.Flow;
using Zenject;

namespace Erziehungsmaschine.Humans
{
    public class HumansInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<HumansModel>().AsSingle();
            Container.Bind<ITickable>().To<HumansController>().AsSingle();
            Container.Bind<HumansController>().AsSingle();

            Container.BindSignal<CutsceneStartedSignal>()
                .To<HumansController>(humansController => humansController.StartSaluting()).AsSingle();
            Container.BindSignal<CutsceneFinishedSignal>()
                .To<HumansController>(humansController => humansController.StopSaluting()).AsSingle();
        }
    }
}