﻿using Slash.Unity.GestureInput.Gestures.Implementations;
using Slash.Unity.GestureInput.Sources;
using UnityEngine;

namespace Erziehungsmaschine.Input
{
    [RequireComponent(typeof(DragSource))]
    public class DragObject : MonoBehaviour
    {
        public DragSource DragSource;

        protected void Awake()
        {
            if (DragSource == null) DragSource = gameObject.AddComponent<DragSource>();
        }

        protected void OnEnable()
        {
            DragSource.BeginDrag.AddListener(OnBeginDrag);
            DragSource.Drag.AddListener(OnDrag);
            DragSource.EndDrag.AddListener(OnEndDrag);
        }

        protected void OnDisable()
        {
            DragSource.BeginDrag.RemoveListener(OnBeginDrag);
            DragSource.Drag.RemoveListener(OnDrag);
            DragSource.EndDrag.RemoveListener(OnEndDrag);
        }

        private void OnEndDrag(EndDragEventData data)
        {
        }

        private void OnDrag(DragEventData data)
        {
            // Convert into world space.
            var delta = ConvertScreenToWorldDistance(data.Delta);
            gameObject.transform.Translate(delta);
        }

        private Vector2 ConvertScreenToWorldDistance(Vector2 distance)
        {
            return distance * Camera.main.orthographicSize * 2 / Screen.height;
        }

        private void OnBeginDrag(BeginDragEventData data)
        {
        }

        protected void Reset()
        {
            if (DragSource == null) DragSource = GetComponent<DragSource>();
        }
    }
}