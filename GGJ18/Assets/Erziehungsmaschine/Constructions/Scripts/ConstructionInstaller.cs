﻿using Erziehungsmaschine.Conversions;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Constructions
{
    public class ConstructionInstaller : MonoInstaller<ConstructionInstaller>
    {
        public Animator Animator;

        public Collider2D Collider;

        public string ConstructionId;

        public Conversion Conversion;

        /// <summary>
        ///     Indicates that a human entering this construction is removed.
        /// </summary>
        public bool RemovesHuman;

        public override void InstallBindings()
        {
            if (Conversion != null)
            {
                Container.BindInstance(Conversion);
            }

            Container.BindInstance(new ConstructionModel
            {
                ConstructionId = ConstructionId,
                IsLocked = true,
                RemovesHuman = RemovesHuman
            });

            Container.BindInterfacesAndSelfTo<ConstructionController>().AsSingle();
            Container.Bind<ConstructionSettings>().AsSingle();

            if (Collider != null)
            {
                Container.BindInstance(Collider);
            }

            if (Animator != null)
            {
                Container.BindInstance(Animator);
                Container.BindInstance(ConstructionAnimator).AsSingle();
            }
        }

        public ConstructionAnimator ConstructionAnimator;
    }
}