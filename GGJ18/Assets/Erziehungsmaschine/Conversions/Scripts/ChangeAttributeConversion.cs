﻿using System;
using Erziehungsmaschine.Humans;

namespace Erziehungsmaschine.Conversions
{
    public abstract class ChangeAttributeConversion<T> : Conversion where T : IConvertible
    {
        private readonly Func<HumanAttributeCollection, T> getAttributeAction;

        private readonly Action<HumanAttributeCollection, T> setAttributeAction;

        /// <summary>
        ///     Value to set attribute to.
        /// </summary>
        public T ChangeAttributeTo;

        protected ChangeAttributeConversion(Action<HumanAttributeCollection, T> setAttributeAction,
            Func<HumanAttributeCollection, T> getAttributeAction)
        {
            this.setAttributeAction = setAttributeAction;
            this.getAttributeAction = getAttributeAction;
        }

        protected abstract T Wildcard { get; }

        public override void Execute(HumanAttributeCollection humanAttributes)
        {
            if (Equals(ChangeAttributeTo, Wildcard))
            {
                // Change to next enum value.
                var enumValues = (T[]) Enum.GetValues(typeof(T));
                var currentValueIndex = Array.IndexOf(enumValues, getAttributeAction(humanAttributes));
                int newValueIndex;
                if (currentValueIndex == -1)
                {
                    newValueIndex = 1;
                }
                else
                {
                    newValueIndex = currentValueIndex + 1;
                    if (newValueIndex >= enumValues.Length)
                    {
                        newValueIndex = 1;
                    }
                }

                setAttributeAction(humanAttributes, enumValues[newValueIndex]);
            }
            else
            {
                setAttributeAction(humanAttributes, ChangeAttributeTo);
            }
        }

        public override string ToString()
        {
            return string.Format("{0}, ChangeAttributeTo: {1}", GetType().Name, ChangeAttributeTo);
        }
    }
}