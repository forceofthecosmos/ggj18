﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumExtensions.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.GestureInput.Utils
{
    using System;

    /// <summary>
    ///   Utility methods for operating on enum values.
    /// </summary>
    public static class EnumExtensions
    {

        /// <summary>
        ///   Replacement for Enum.HasFlag in .NET before 4.0.
        /// </summary>
        /// <param name="value">Value to check.</param>
        /// <param name="option">Option which is checked.</param>
        /// <returns>True if the specified option is set for the specified value.</returns>
        public static bool IsOptionSet(this Enum value, Enum option)
        {
            if (IsSignedEnumValue(value))
            {
                var longVal = Convert.ToInt64(value);
                var longOpt = Convert.ToInt64(option);
                return (longVal & longOpt) == longOpt;
            }
            else
            {
                var longVal = Convert.ToUInt64(value);
                var longOpt = Convert.ToUInt64(option);
                return (longVal & longOpt) == longOpt;
            }
        }

        /// <summary>
        ///   Checks if the type of the specified enum value is a signed type.
        /// </summary>
        /// <param name="enumValue">Enum value to check the type of.</param>
        /// <returns>
        ///   <c>true</c>, if the type of the specified enum value is a signed type, and <c>false</c> otherwise.
        /// </returns>
#if NETFX_CORE
        private static bool IsSignedEnumValue(object enumValue)
        {
            var enumType = enumValue.GetType();

            return !(enumType == typeof(byte) || enumType == typeof(ushort) || enumType == typeof(uint)
                    || enumType == typeof(ulong));
        }
#else
        private static bool IsSignedEnumValue(IConvertible enumValue)
        {
            switch (enumValue.GetTypeCode())
            {
                case TypeCode.Byte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return false;
                default:
                    return true;
            }
        }
#endif
    }
}