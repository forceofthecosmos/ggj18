﻿using Erziehungsmaschine.Humans;

namespace Erziehungsmaschine.Conversions
{
    public class ChangeHeadAttributeConversion : ChangeAttributeConversion<HumanHeadAttribute>
    {
        public ChangeHeadAttributeConversion() : base((attributes, value) => attributes.HeadAttribute = value,
            attributes => attributes.HeadAttribute)
        {
        }

        protected override HumanHeadAttribute Wildcard
        {
            get { return HumanHeadAttribute.Any; }
        }
    }
}