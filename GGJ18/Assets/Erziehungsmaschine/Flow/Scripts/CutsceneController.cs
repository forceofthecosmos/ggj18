﻿using System;
using Erziehungsmaschine.Constructions.Signals;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class CutsceneController : IInitializable, IDisposable
    {
        private readonly CutsceneFinishedSignal cutsceneFinishedSignal;

        private readonly CutsceneStartedSignal cutsceneStartedSignal;

        private readonly CutsceneModel model;

        private readonly CutsceneVisualizer view;

        private TriggerCutsceneSignal triggerCutsceneSignal;

        private readonly UnlockConstructionSignal unlockConstructionSignal;

        [Inject]
        public CutsceneController(CutsceneModel model, CutsceneVisualizer view,
            TriggerCutsceneSignal triggerCutsceneSignal, CutsceneStartedSignal cutsceneStartedSignal,
            CutsceneFinishedSignal cutsceneFinishedSignal, UnlockConstructionSignal unlockConstructionSignal)
        {
            this.model = model;
            this.view = view;
            this.triggerCutsceneSignal = triggerCutsceneSignal;
            this.cutsceneStartedSignal = cutsceneStartedSignal;
            this.cutsceneFinishedSignal = cutsceneFinishedSignal;
            this.unlockConstructionSignal = unlockConstructionSignal;
        }

        public void ContinueCutscene()
        {
            if (model.Cutscene == null)
            {
                Debug.LogWarning("No cutscene set, can't continue");
                return;
            }

            ++model.CutsceneStepIndex;
            PlayCutsceneStep(model.Cutscene, model.CutsceneStepIndex);
        }

        public void Dispose()
        {
            triggerCutsceneSignal -= PlayCutscene;
        }

        public void Initialize()
        {
            triggerCutsceneSignal += PlayCutscene;
        }

        private void PlayCutscene(Cutscene cutscene, Action onCutsceneFinished)
        {
            model.Cutscene = cutscene;
            model.CutsceneStepIndex = -1;
            model.CutsceneFinishedCallback = onCutsceneFinished;
            PlayCutsceneStep(model.Cutscene, model.CutsceneStepIndex);

            cutsceneStartedSignal.Fire();
        }

        private void PlayCutsceneStep(Cutscene cutscene, int stepIndex)
        {
            if (stepIndex < 0)
            {
                // Fade in.
                view.FadeIn();
            }
            else if (stepIndex < cutscene.Steps.Count)
            {
                var cutsceneStep = cutscene.Steps[stepIndex];

                // Unlock construction if set.
                if (!string.IsNullOrEmpty(cutsceneStep.UnlockConstructionId))
                {
                    unlockConstructionSignal.Fire(cutsceneStep.UnlockConstructionId);
                }

                view.ShowText(cutsceneStep.Text);
            }
            else if (stepIndex == cutscene.Steps.Count)
            {
                // Fade out.
                view.FadeOut();
            }
            else
            {
                model.Cutscene = null;
                model.OnCutsceneFinished();
                cutsceneFinishedSignal.Fire();
            }
        }
    }
}