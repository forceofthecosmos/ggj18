﻿using System;
using System.Collections.Generic;
using Erziehungsmaschine.Flow;
using Erziehungsmaschine.Humans;
using UnityEngine;

namespace Erziehungsmaschine.Level
{
    [Serializable]
    public class LevelGoalConfig
    {
        public HumanAttributeCollection NeededAttributes;

        public int NeededCount;
    }

    /// <summary>
    ///     Defines which humans are needed for this level
    /// </summary>
    public class LevelConfigScriptableObject : ScriptableObject
    {
        public Cutscene Cutscene;

        /// <summary>
        ///   Goals for level.
        /// </summary>
        public List<LevelGoalConfig> Goals;

        /// <summary>
        ///     Time between spawning a human (in s).
        /// </summary>
        [Tooltip("Human Spawn Rate, i.e. Time between spawning a human (in s)")]
        [Range(1, 30)]
        public float HumanSpawnTime = 5;
        
        public LevelConfigScriptableObject NextLevel;

        public Cutscene Outro;
    
        public int WrongHumanFailCount;
    }
}