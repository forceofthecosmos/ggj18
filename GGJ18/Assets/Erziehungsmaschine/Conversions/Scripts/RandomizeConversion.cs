﻿using Erziehungsmaschine.Humans;

namespace Erziehungsmaschine.Conversions
{
    public class RandomizeConversion : Conversion
    {
        /// <inheritdoc />
        public override void Execute(HumanAttributeCollection humanAttributes)
        {
            humanAttributes.Randomize();
        }
    }
}