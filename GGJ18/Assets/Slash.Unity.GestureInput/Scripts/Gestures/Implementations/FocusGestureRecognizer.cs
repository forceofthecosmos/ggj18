﻿namespace Slash.Unity.GestureInput.Gestures.Implementations
{
    using System;
    using System.Collections.Generic;

    using Slash.Unity.GestureInput.Devices;
    using Slash.Unity.GestureInput.Modules;
    using Slash.Unity.GestureInput.Raycasts;
    using Slash.Unity.GestureInput.Sources;
    using Slash.Unity.GestureInput.Utils;

    using UnityEngine;

    /// <summary>
    ///   Monitors the game object which is currently focused by a specific pointer.
    ///   Informs about gaining and loosing the focus.
    /// </summary>
    [Serializable]
    public class FocusGestureRecognizer : GestureRecognizer
    {
        /// <summary>
        ///   Current focused game object.
        /// </summary>
        private GameObject focusedGameObject;

        /// <summary>
        ///   Pointer to use screen position from.
        /// </summary>
        private IPointerDevice pointerDevice;

        /// <summary>
        ///   Detection to use to get focused game object.
        /// </summary>
        private IPointerTouchDetection pointerTouchDetection;

        /// <inheritdoc />
        public override IEnumerable<RegisteredInputSource> GetInputSources()
        {
            yield return
                new RegisteredInputSource<FocusSource, FocusGainedEventData>(
                    (source, eventData) => source.OnFocusGained(eventData));
            yield return
                new RegisteredInputSource<FocusSource, FocusLostEventData>(
                    (source, eventData) => source.OnFocusLost(eventData));
        }

        /// <summary>
        ///   Initializes the recognizer.
        /// </summary>
        /// <param name="pointerDevice">Pointer to use screen position from.</param>
        /// <param name="pointerTouchDetection">Detection to use to get focused game object.</param>
        public void Init(IPointerDevice pointerDevice, IPointerTouchDetection pointerTouchDetection)
        {
            this.pointerTouchDetection = pointerTouchDetection;
            this.pointerDevice = pointerDevice;
        }

        /// <inheritdoc />
        public override void Process()
        {
            if (this.pointerDevice == null || this.pointerTouchDetection == null)
            {
                return;
            }

            // Check if focused game object changed.
            var newFocusedGameObject = this.pointerTouchDetection.GetTouchedGameObject(this.pointerDevice.GetPosition());
            if (newFocusedGameObject == this.focusedGameObject)
            {
                return;
            }

            // Handle focus lost.
            if (this.focusedGameObject != null)
            {
                this.OnGestureDetected(new FocusLostEventData(), new[] { this.focusedGameObject });
            }

            this.focusedGameObject = newFocusedGameObject;

            // Handle focus gained.
            if (this.focusedGameObject != null)
            {
                this.OnGestureDetected(new FocusGainedEventData(), new[] { this.focusedGameObject });
            }
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var debugText = string.Empty;
            if (this.focusedGameObject != null)
            {
                debugText += string.Format("Focused: {0}", this.focusedGameObject.GetPath());
            }
            return debugText;
        }
    }

    public class FocusGainedEventData : IGestureData
    {
    }

    public class FocusLostEventData : IGestureData
    {
    }
}