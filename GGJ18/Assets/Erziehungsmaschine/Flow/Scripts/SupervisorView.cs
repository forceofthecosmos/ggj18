﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Erziehungsmaschine.Flow
{
    public class SupervisorView : MonoBehaviour
    {
        private CutsceneController controller;

        public void Continue()
        {
            OnContinue();
        }

        [Inject]
        public void Initialize(CutsceneController controllerInject)
        {
            controller = controllerInject;
        }

        public void OnFadeInFinished()
        {
            OnContinue();
        }

        public void OnFadeOutFinished()
        {
            OnContinue();
        }

        private void OnContinue()
        {
            controller.ContinueCutscene();
        }
    }
}