﻿using System;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Constructions
{
    public class ConstructionAnimator : MonoBehaviour, IInitializable, IDisposable
    {
        private const string IsProcessingParameterName = "Is Processing";

        private Animator animator;

        private Collider2D collider;

        private ConstructionModel model;

        public GameObject VisualizationRoot;

        [Inject]
        public void Initialize(ConstructionModel model, Animator animator, Collider2D collider)
        {
            this.model = model;
            this.animator = animator;
            this.collider = collider;

            Initialize();
        }

        public void Dispose()
        {
            model.LockedStateChanged -= OnLockedStateChanged;
            model.ProcessChanged -= OnProcessChanged;
        }

        public void Initialize()
        {
            model.LockedStateChanged += OnLockedStateChanged;
            model.ProcessChanged += OnProcessChanged;

            // Init from model.
            OnLockedStateChanged();
        }
        
        private void OnLockedStateChanged()
        {
            if (VisualizationRoot != null)
            {
                VisualizationRoot.SetActive(!model.IsLocked);
            }

            collider.enabled = !model.IsLocked;
        }

        private void OnProcessChanged()
        {
            // Update animator.
            if (animator != null)
            {
                animator.SetBool(IsProcessingParameterName, model.ConversionProcess != null);
            }
        }
    }
}