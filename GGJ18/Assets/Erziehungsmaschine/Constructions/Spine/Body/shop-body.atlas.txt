
shop-body.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
shop-body-belt
  rotate: false
  xy: 2, 291
  size: 425, 232
  orig: 425, 232
  offset: 0, 0
  index: -1
shop-body-skirt-left
  rotate: false
  xy: 452, 620
  size: 315, 291
  orig: 315, 291
  offset: 0, 0
  index: -1
shop-body-skirt-right
  rotate: false
  xy: 2, 525
  size: 448, 386
  orig: 448, 386
  offset: 0, 0
  index: -1
shop-body-stairs
  rotate: true
  xy: 769, 644
  size: 267, 142
  orig: 267, 142
  offset: 0, 0
  index: -1
shop-body-top
  rotate: false
  xy: 2, 18
  size: 310, 271
  orig: 310, 271
  offset: 0, 0
  index: -1
