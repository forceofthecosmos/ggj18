﻿using System;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Humans
{
    public class HumanAnimator : IInitializable, IDisposable
    {
        private readonly Animator animator;

        private readonly HumanModel model;

        [Inject]
        public HumanAnimator(HumanModel model, Animator animator)
        {
            this.model = model;
            this.animator = animator;
        }

        public void Dispose()
        {
            model.SalutingStateChanged -= OnSalutingStateChanged;
            model.DraggedStateChanged -= OnDraggedStateChanged;
            model.WalkingStateChanged -= OnWalkingStateChanged;
        }

        private void OnWalkingStateChanged()
        {
            UpdateAnimatorState();
        }


        public void Initialize()
        {
            model.SalutingStateChanged += OnSalutingStateChanged;
            model.DraggedStateChanged += OnDraggedStateChanged;
            model.WalkingStateChanged += OnWalkingStateChanged;
        }

        private void OnDraggedStateChanged()
        {
            UpdateAnimatorState();
        }

        private void OnSalutingStateChanged()
        {
            UpdateAnimatorState();
        }

        public void UpdateAnimatorState()
        {
            animator.SetBool("Is Saluting", model.IsSaluting);
            animator.SetBool("Is Dragged", model.IsDragged);
            animator.SetBool("Is Walking", model.IsWalking);
            animator.SetBool("IsCarrying", model.IsCarrying);
        }
    }
}