namespace Slash.Unity.GestureInput.Gestures.Pinch
{
    using System;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class PinchSource : MonoBehaviour, IEventSystemHandler
    {
        public PinchEvent Pinch;

        public bool OnPinch(PinchEventData eventData)
        {
            Pinch.Invoke(eventData);
            return true;
        }

        [Serializable]
        public class PinchEvent : UnityEvent<PinchEventData>
        {
        }
    }
}