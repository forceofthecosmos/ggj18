﻿using System;
using UnityEngine;

namespace Erziehungsmaschine.Constructions
{
    [Serializable]
    public class ConstructionSettings
    {
        /// <summary>
        ///     Duration to convert a human in this construction (in s).
        /// </summary>
        [Tooltip("Duration to convert a human in this construction (in s)")]
        public float ConversionDuration = 5.0f;

        /// <summary>
        ///     Position where a human exits the construction (in world units).
        /// </summary>
        [Tooltip("Position where a human exits the construction (in world units)")]
        public Vector2 ExitPosition;
    }
}