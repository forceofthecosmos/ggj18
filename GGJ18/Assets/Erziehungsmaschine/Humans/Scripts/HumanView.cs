using System;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace Erziehungsmaschine.Humans
{
    public class HumanView : MonoBehaviour, IDisposable
    {
        public event Action<HumanView> AgentReachedDestination;

        [SerializeField] private NavMeshAgent agent;

        [SerializeField] private SkeletonAnimator skeleton;

        public HumanModel Model { get; private set; }

        public float TargetReachedMargin = 3;

        private HumanAnimator humanAnimator;

        public HumanController Controller { get; private set; }

        public void Dispose()
        {
            SetModel(null);
        }

        [Inject]
        public void Initialize(HumanController controller, HumanModel model, HumanAnimator animator)
        {
            humanAnimator = animator;
            Controller = controller;
            SetModel(model);

            if (agent != null)
            {
                agent.updateUpAxis = false;
                agent.updateRotation = false;
            }
        }

        private void Start()
        {
            humanAnimator.UpdateAnimatorState();
        }

        private void Update()
        {
            if (agent != null && agent.pathPending == false && agent.isOnNavMesh)
            {
                if (agent.remainingDistance <= agent.stoppingDistance + TargetReachedMargin)
                {
                    if (!agent.hasPath || Math.Abs(agent.velocity.sqrMagnitude) < Mathf.Epsilon)
                    {
                        // Done
                        FireAgentReachedDestination(this);
                    }
                }
            }

            skeleton.GetComponent<MeshRenderer>().sortingOrder = (int)(transform.position.y * 100 * -1);
        }

        private void OnTeleport(Vector2 worldPosition)
        {
            transform.position = worldPosition;
        }

        public void SetNavigationEnabled(bool isEnabled)
        {
            agent.enabled = isEnabled;
            if (isEnabled) agent.SetDestination(Model.TargetPosition);
        }

        protected virtual void FireAgentReachedDestination(HumanView obj)
        {
            Action<HumanView> handler = AgentReachedDestination;
            if (handler != null) handler(obj);
        }

        private void SetModel(HumanModel model)
        {
            if (Model != null)
            {
                Model.Teleport -= OnTeleport;
                Model.TargetPositionChanged -= OnTargetPositionChange;
                Model.WalkingStateChanged -= OnWalkingStateChanged;
            }

            Model = model;

            if (Model != null)
            {
                Model.Teleport += OnTeleport;
                Model.TargetPositionChanged += OnTargetPositionChange;
                Model.WalkingStateChanged += OnWalkingStateChanged;

                // Init from model.
                OnWalkingStateChanged();
                UpdateAttributeSprites(skeleton.Skeleton, Model.Attributes);
            }
        }

        private void OnTargetPositionChange()
        {
            if (agent != null && agent.isOnNavMesh)
            {
                agent.SetDestination(Model.TargetPosition);
            }
        }

        private void OnWalkingStateChanged()
        {
            agent.enabled = Model.IsWalking;
            if (Model.IsWalking)
            {
                if (agent.isOnNavMesh)
                {
                    agent.SetDestination(Model.TargetPosition);
                }
            }
        }

        public static void UpdateAttributeSprites(Skeleton skeleton, HumanAttributeCollection attributes)
        {
            UpdateBodyAttribute(skeleton, attributes);
            UpdateHeadAttribute(skeleton, attributes);
            UpdateFaceAttribute(skeleton, attributes);
            UpdateObjectAttribute(skeleton, attributes);
        }

        private static void UpdateHeadAttribute(Skeleton skeleton, HumanAttributeCollection attributes)
        {
            string skinName = string.Empty;
            switch (attributes.HeadAttribute)
            {
                case HumanHeadAttribute.Any:
                    skinName = "hat-random";
                    break;
                case HumanHeadAttribute.Default:
                    skinName = "hat-default";
                    break;
                case HumanHeadAttribute.Snob:
                    skinName = "hat-snob";
                    break;
                case HumanHeadAttribute.Folk:
                    skinName = "hat-folk";
                    break;
                case HumanHeadAttribute.Farmer:
                    skinName = "hat-farmer";
                    break;
                case HumanHeadAttribute.Office:
                    skinName = "hat-office";
                    break;
                case HumanHeadAttribute.Soilder:
                    skinName = "hat-soldier";
                    break;
            }

            ApplySkin(skeleton, skinName);
        }
        
        private static void UpdateBodyAttribute(Skeleton skeleton, HumanAttributeCollection attributes)
        {
            string skinName = string.Empty;
            switch (attributes.BodyAttribute)
            {
                case HumanBodyAttribute.Any:
                    skinName = "body-random";
                    break;
                case HumanBodyAttribute.Default:
                    skinName = "body-default";
                    break;
                case HumanBodyAttribute.Snob:
                    skinName = "body-snob";
                    break;
                case HumanBodyAttribute.Folk:
                    skinName = "body-folk";
                    break;
                case HumanBodyAttribute.Farmer:
                    skinName = "body-farmer";
                    break;
                case HumanBodyAttribute.Office:
                    skinName = "body-office";
                    break;
                case HumanBodyAttribute.Soilder:
                    skinName = "body-soldier";
                    break;
            }

            ApplySkin(skeleton, skinName);
        }

        private static void UpdateFaceAttribute(Skeleton skeleton, HumanAttributeCollection attributes)
        {
            string skinName = string.Empty;
            switch (attributes.FaceAttribute)
            {
                case HumanFaceAttribute.Any:
                    skinName = "face-random";
                    break;
                case HumanFaceAttribute.Default:
                    skinName = "face-default";
                    break;
                case HumanFaceAttribute.Snob:
                    skinName = "face-snob";
                    break;
                case HumanFaceAttribute.Folk:
                    skinName = "face-folk";
                    break;
                case HumanFaceAttribute.Farmer:
                    skinName = "face-farmer";
                    break;
                case HumanFaceAttribute.Office:
                    skinName = "face-office";
                    break;
                case HumanFaceAttribute.Soilder:
                    skinName = "face-soldier";
                    break;
            }

            ApplySkin(skeleton, skinName);
        }
        
        private static void UpdateObjectAttribute(Skeleton skeleton, HumanAttributeCollection attributes)
        {
            string skinName = string.Empty;
            switch (attributes.ObjectAttribute)
            {
                case HumanObjectAttribute.Any:
                    skinName = "object-random";
                    break;
                case HumanObjectAttribute.Default:
                    skinName = "object-default";
                    break;
                case HumanObjectAttribute.Snob:
                    skinName = "object-snob";
                    break;
                case HumanObjectAttribute.Folk:
                    skinName = "object-folk";
                    break;
                case HumanObjectAttribute.Farmer:
                    skinName = "object-farmer";
                    break;
                case HumanObjectAttribute.Office:
                    skinName = "object-office";
                    break;
                case HumanObjectAttribute.Soilder:
                    skinName = "object-soldier";
                    break;
            }

            ApplySkin(skeleton, skinName);
        }
        
        private static void ApplySkin(Skeleton skeleton, string skinName)
        {
            if (string.IsNullOrEmpty(skinName) == false)
            {
                var skin = skeleton.Data.FindSkin(skinName);
                foreach (var skinAttachment in skin.Attachments)
                {
                    var slot = skeleton.FindSlot(skinAttachment.Key.name);
                    slot.Attachment = skinAttachment.Value;
                }
            }
        }
    }
}