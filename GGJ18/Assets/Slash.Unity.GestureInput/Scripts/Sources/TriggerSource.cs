﻿namespace Slash.Unity.GestureInput.Sources
{
    using System;
    using System.Collections.Generic;
    using Slash.Unity.GestureInput.Gestures.Implementations;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class TriggerSource : MonoBehaviour, IEventSystemHandler
    {
        /// <summary>
        ///     Indicates that a valid trigger event is not consumed, but passed onwards.
        /// </summary>
        [Tooltip("Indicates that a valid trigger event is not consumed, but passed onwards")]
        public bool ShareEvent;

        public TriggerEvent Trigger = new TriggerEvent();

        /// <summary>
        ///     Buttons which will fire the trigger.
        /// </summary>
        [Tooltip("Buttons which will fire the trigger")]
        public List<string> ValidButtons;

        public bool OnTrigger(TriggerEventData eventData)
        {
            if (this.ValidButtons.Count != 0 && !this.ValidButtons.Contains(eventData.Button))
            {
                return false;
            }

            this.Trigger.Invoke();
            return !this.ShareEvent;
        }

        [Serializable]
        public class TriggerEvent : UnityEvent
        {
        }
    }
}