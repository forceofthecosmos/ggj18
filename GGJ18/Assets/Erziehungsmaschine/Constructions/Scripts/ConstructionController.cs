﻿using System;
using Erziehungsmaschine.Constructions.Signals;
using Erziehungsmaschine.Conversions;
using Erziehungsmaschine.Humans;
using UnityEngine;
using Zenject;

namespace Erziehungsmaschine.Constructions
{
    public class ConstructionController : IInitializable, IDisposable
    {
        private readonly ConstructionModel model;

        private readonly ConstructionSettings settings;

        [Inject] private HumansController humansController;

        [Inject] private TickableManager tickableManger;

        private UnlockConstructionSignal unlockConstructionSignal;

        [Inject]
        private Conversion conversion;

        [Inject]
        public ConstructionController(ConstructionSettings settings, ConstructionModel model, UnlockConstructionSignal unlockConstructionSignal)
        {
            this.settings = settings;
            this.model = model;
            this.unlockConstructionSignal = unlockConstructionSignal;
        }

        public void DropHuman(HumanController human)
        {
            Debug.LogFormat("Dropped human '{0}' on construction '{1}'", human.Model, model);

            if (model.ConversionProcess != null)
            {
                Debug.LogFormat("Can't start conversion of human '{0}', construction '{1}' already occupied", human.Model,
                    model);
                human.SetPosition(settings.ExitPosition);
                return;
            }

            // Start the conversion of this human.
            model.ConversionProcess = StartConversion(human.Model);
        }

        private ConversionProcess StartConversion(HumanModel human)
        {
            var conversionProcess = new ConversionProcess(human, settings.ConversionDuration, OnConversionFinished, this.conversion);
            tickableManger.AddFixed(conversionProcess);

            // Remove human from game world.
            humansController.Despawn(human);

            Debug.LogFormat("Conversion started: {0}", conversionProcess);

            return conversionProcess;
        }

        private void FinishConversion(ConversionProcess conversionProcess)
        {
            Debug.LogFormat("Conversion in '{0}' finished: '{1}'", model.ConstructionId, model.ConversionProcess);

            if (!model.RemovesHuman)
            {
                // Add human to game world at construction exit.
                var human = conversionProcess.Human;
                humansController.Spawn(human, settings.ExitPosition);
            }

            tickableManger.RemoveFixed(conversionProcess);
        }

        private void OnConversionFinished()
        {
            FinishConversion(model.ConversionProcess);

            // Clear conversion process.
            model.ConversionProcess = null;
        }

        public void Initialize()
        {
            unlockConstructionSignal += OnUnlockConstruction;
        }

        private void OnUnlockConstruction(string constructionId)
        {
            // Check if this construction should be unlocked.
            if (model.ConstructionId == constructionId)
            {
                model.IsLocked = false;
            }
        }

        public void Dispose()
        {
            unlockConstructionSignal -= OnUnlockConstruction;
        }
    }
}