﻿using UnityEditor;
using UnityEditor.AI;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(NavMeshSurface2D))]
public class NavMeshSurface2DEditor : NavMeshSurfaceEditor
{
    private SerializedProperty polygonColliders;

    private Quaternion correctRotation;

    protected override void OnEnable()
    {
        base.OnEnable();
        polygonColliders = serializedObject.FindProperty("polygonColliders");
        correctRotation = Quaternion.Euler(-90, 0, 0);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        EditorGUILayout.PropertyField(polygonColliders, true);

        serializedObject.ApplyModifiedProperties();

        var comp = (NavMeshSurface2D)target;
        if (comp.transform.rotation != correctRotation)
        {
            EditorGUILayout.HelpBox("Rotation is not correct. It should be (-90, 0, 0) to work!", MessageType.Warning);
        }
    }
}
