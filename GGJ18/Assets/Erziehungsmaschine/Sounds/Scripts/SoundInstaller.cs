﻿using Zenject;

namespace Erziehungsmaschine.Sounds.Scripts
{
    public class SoundInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<SoundController>().AsSingle();
        }
    }
}