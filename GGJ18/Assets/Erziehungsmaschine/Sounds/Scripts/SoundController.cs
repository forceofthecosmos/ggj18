﻿using Erziehungsmaschine.Sounds.Scripts;
using UnityEngine;

public class SoundController
{
    private readonly SoundSettingsScriptableObject.AudioSettings audioSettings;

    private readonly AudioSource atmoAudioSource;

    private readonly AudioSource sfxAudioSource;

    public SoundController(SoundSettingsScriptableObject.AudioSettings audioSettings)
    {
        this.audioSettings = audioSettings;
        
        var obj = new GameObject("AtmoAudio");
        Object.DontDestroyOnLoad(obj);
        atmoAudioSource = obj.AddComponent<AudioSource>();
        
        var sfxObj = new GameObject("SFXAudio");
        Object.DontDestroyOnLoad(sfxObj);
        sfxAudioSource = sfxObj.AddComponent<AudioSource>();
        sfxAudioSource.volume = 0.5f;
    }

    public void PlayClip(AudioClip clip)
    {
        sfxAudioSource.PlayOneShot(clip);
    }

    public void PlayAtmoClip(bool loop)
    {
        atmoAudioSource.clip =
            loop ? audioSettings.MainThemeLoop : audioSettings.MainTheme;
        atmoAudioSource.loop = loop;
        atmoAudioSource.Play();
    }

    public void StopAtmo()
    {
        atmoAudioSource.Stop();
    }
}
