﻿namespace Erziehungsmaschine.Humans
{
    public enum HumanHeadAttribute
    {
        Any,

        Default,
        Snob,
        Folk,
        Farmer,
        Office,
        Soilder,
    }

    public enum HumanFaceAttribute
    {
        Any,

        Default,
        Snob,
        Folk,
        Farmer,
        Office,
        Soilder,
    }

    public enum HumanBodyAttribute
    {
        Any,

        Default,
        Snob,
        Folk,
        Farmer,
        Office,
        Soilder,
    }

    public enum HumanObjectAttribute
    {
        Any,

        Default,
        Snob,
        Folk,
        Farmer,
        Office,
        Soilder,
    }

    public enum HumanMindAttribute
    {
        Any,

        Default,
        Snob,
        Folk,
        Farmer,
        Office,
        Soilder,
    }
}