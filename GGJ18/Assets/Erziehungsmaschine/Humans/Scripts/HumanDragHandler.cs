﻿using System;
using Erziehungsmaschine.Sounds.Scripts;
using Slash.Unity.GestureInput.Gestures.Implementations;
using Slash.Unity.GestureInput.Sources;
using Zenject;

namespace Erziehungsmaschine.Humans
{
    public class HumanDragHandler : IInitializable, IDisposable
    {
        private readonly DragSource dragSource;

        private readonly SoundController soundController;

        private readonly SoundSettingsScriptableObject.AudioSettings audioSettings;

        private readonly HumanModel model;

        public HumanDragHandler(HumanModel model, DragSource dragSource, SoundController soundController, SoundSettingsScriptableObject.AudioSettings audioSettings)
        {
            this.model = model;
            this.dragSource = dragSource;
            this.soundController = soundController;
            this.audioSettings = audioSettings;
        }

        public void Dispose()
        {
            dragSource.BeginDrag.RemoveListener(OnBeginDrag);
            dragSource.EndDrag.RemoveListener(OnEndDrag);
        }

        public void Initialize()
        {
            dragSource.BeginDrag.AddListener(OnBeginDrag);
            dragSource.EndDrag.AddListener(OnEndDrag);
        }

        private void OnBeginDrag(BeginDragEventData data)
        {
            soundController.PlayClip(audioSettings.Yay);
            model.IsWalking = false;
            model.IsDragged = true;
        }

        private void OnEndDrag(EndDragEventData data)
        {
            model.IsDragged = false;
            model.IsWalking = true;
        }
    }
}