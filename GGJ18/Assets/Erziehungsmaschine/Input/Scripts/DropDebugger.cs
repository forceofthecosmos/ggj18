﻿using Slash.Unity.GestureInput.Dragging;
using UnityEngine;

namespace Erziehungsmaschine.Input
{
    public class DropDebugger : MonoBehaviour
    {
        public void OnDrop(DropEventData data)
        {
            Debug.LogFormat(this, "Dropped on this object: {0}", data);
        }
    }
}